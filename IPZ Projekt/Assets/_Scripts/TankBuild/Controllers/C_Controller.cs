﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Controller : MonoBehaviour
{
    public float m_leftTrackOutputSignal;
    public float m_rightTrackOutputSignal;

    public List<C_Regulation> m_leftTrackRegulators;
    public List<C_Regulation> m_rightTrackRegulators;
    public C_Regulation m_towerReg;
    public C_Regulation m_cannonReg;

    public float m_towerAngle;
    public float m_cannonAngle;
    public enum Mode { manual = 1, semiauto = 2, auto = 3 }
    public Mode m_mode;

    public C_Camera m_camera;

    public Transform m_destination;
    public void M_SendSignals()
    {
        foreach (C_Regulation obj in m_leftTrackRegulators)
            obj.M_ProcessSignal(m_leftTrackOutputSignal);
        foreach (C_Regulation obj in m_rightTrackRegulators)
            obj.M_ProcessSignal(m_rightTrackOutputSignal);
        m_towerReg.M_ProcessSignal(m_towerAngle);
        m_cannonReg.M_ProcessSignal(m_cannonAngle);
    }
    public void M_AttachComponents(List<C_Regulation> leftTrackRegs, List<C_Regulation> rightTrackRegs)
    {
        m_leftTrackRegulators = leftTrackRegs;
        m_rightTrackRegulators = rightTrackRegs;
    }

    virtual public void M_CalculatedSignal()
    {

    }
    public void M_Process()
    {
        M_CalculatedSignal();
        M_SendSignals();
    }
}