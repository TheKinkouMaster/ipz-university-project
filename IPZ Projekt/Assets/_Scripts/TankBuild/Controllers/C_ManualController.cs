﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_ManualController : C_Controller {
    public float m_leftTrackSpeed;
    public float m_rightTrackSpeed;

    public float m_speedJump;

    public GameObject m_bulletpf;
    public Transform m_bulletSpawn;
    public float m_bulletSpeed;

    public float m_towerAngleJump;
    public float m_cannonAngleJump;

   
    public override void M_CalculatedSignal()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            m_mode = Mode.manual;
            m_camera.M_SimToTPS(C_Camera.CameraMode.tank);
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            m_mode = Mode.semiauto;
            m_camera.M_SimToTPS(C_Camera.CameraMode.tank);
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            m_mode = Mode.auto;
            m_camera.M_SimToTPS(C_Camera.CameraMode.tankTPS);
        }
        if (m_mode == Mode.manual)
        {
            //Lewa gasienica
            if (Input.GetKey(KeyCode.W))
                m_leftTrackOutputSignal = 1;
            else
                m_leftTrackOutputSignal = 0;

            //Prawa gasienica
            if (Input.GetKey(KeyCode.E))
                m_rightTrackOutputSignal = 1;
            else
                m_rightTrackOutputSignal = 0;

            //Zmiana predkosci
            if (Input.GetKey(KeyCode.Q))
                m_leftTrackSpeed += m_speedJump;
            else if (Input.GetKey(KeyCode.A))
                m_leftTrackSpeed -= m_speedJump;

            if (Input.GetKey(KeyCode.R))
                m_rightTrackSpeed += m_speedJump;
            else if (Input.GetKey(KeyCode.F))
                m_rightTrackSpeed -= m_speedJump;

            //wiezyczka
            if (Input.GetKey(KeyCode.Keypad4))
                m_towerAngle -= m_towerAngleJump;
            else if (Input.GetKey(KeyCode.Keypad6))
                m_towerAngle += m_towerAngleJump;

            //działo
            if (Input.GetKey(KeyCode.Keypad8))
                m_cannonAngle -= m_cannonAngleJump;
            else if (Input.GetKey(KeyCode.Keypad2))
                m_cannonAngle += m_cannonAngleJump;
            if (Input.GetKeyDown(KeyCode.Keypad5))
            {
                GameObject obj = Instantiate(m_bulletpf);
                obj.transform.position = m_bulletSpawn.position;
                obj.transform.rotation = m_bulletSpawn.rotation;
                obj.GetComponent<Rigidbody>().velocity = obj.transform.forward * m_bulletSpeed;
            }

        }
         if (m_mode == Mode.semiauto)
        {
            //Lewa gasienica
            if (Input.GetKey(KeyCode.W))
            {
                m_leftTrackOutputSignal = 1;
                m_rightTrackOutputSignal = 1;
                m_leftTrackSpeed = 1;
                m_rightTrackSpeed = 1;
            }
            if (Input.GetKey(KeyCode.S))
            {
                m_leftTrackOutputSignal = 1;
                m_rightTrackOutputSignal = 1;
                m_leftTrackSpeed = -1;
                m_rightTrackSpeed = -1;
            }
            if (Input.GetKey(KeyCode.A))
            {
                m_leftTrackOutputSignal = 1;
                m_rightTrackOutputSignal = 1;
                m_leftTrackSpeed = 0.25f;
                m_rightTrackSpeed = 1;
            }
            if (Input.GetKey(KeyCode.D))
            {
                m_leftTrackOutputSignal = 1;
                m_rightTrackOutputSignal = 1;
                m_leftTrackSpeed = 1;
                m_rightTrackSpeed = 0.25f;
            }
            if (Input.GetKey(KeyCode.Q))
            {
                m_leftTrackOutputSignal = 1;
                m_rightTrackOutputSignal = 1;
                m_leftTrackSpeed = -1;
                m_rightTrackSpeed = 1;
            }
            if (Input.GetKey(KeyCode.E))
            {
                m_leftTrackOutputSignal = 1;
                m_rightTrackOutputSignal = 1;
                m_leftTrackSpeed = 1;
                m_rightTrackSpeed = -1;
            }
            if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.Q) && !Input.GetKey(KeyCode.E))
            {
                m_leftTrackOutputSignal = 0;
                m_rightTrackOutputSignal = 0;
            }

            //wiezyczka
            if (Input.GetKey(KeyCode.Keypad4))
                m_towerAngle -= m_towerAngleJump;
            else if (Input.GetKey(KeyCode.Keypad6))
                m_towerAngle += m_towerAngleJump;

            //działo
            if (Input.GetKey(KeyCode.Keypad8))
                m_cannonAngle -= m_cannonAngleJump;
            else if (Input.GetKey(KeyCode.Keypad2))
                m_cannonAngle += m_cannonAngleJump;
            if (Input.GetKeyDown(KeyCode.Keypad5))
            {
                GameObject obj = Instantiate(m_bulletpf);
                obj.transform.position = m_bulletSpawn.position;
                obj.transform.rotation = m_bulletSpawn.rotation;
                obj.GetComponent<Rigidbody>().velocity = obj.transform.forward * m_bulletSpeed;
            }

        }
        if (m_mode == Mode.auto)
        {
            

        }

        //sprawdzanie wartosci
        M_CheckPower();
        M_CheckCannonAngle();

        //obliczanie sygnalu na wyjsciu
        m_leftTrackOutputSignal = m_leftTrackSpeed * m_leftTrackOutputSignal;
        m_rightTrackOutputSignal = m_rightTrackSpeed * m_rightTrackOutputSignal;
    }
    public void M_CheckCannonAngle()
    {
        if (m_cannonAngle < -25) m_cannonAngle = -25;
        if (m_cannonAngle > 5) m_cannonAngle = 5;
    }
    public void M_CheckPower()
    {
        if (m_leftTrackSpeed > 1) m_leftTrackSpeed = 1;
        else if (m_leftTrackSpeed < -1) m_leftTrackSpeed = -1;
        if (m_rightTrackSpeed > 1) m_rightTrackSpeed = 1;
        else if (m_rightTrackSpeed < -1) m_rightTrackSpeed = -1;
    }
}
