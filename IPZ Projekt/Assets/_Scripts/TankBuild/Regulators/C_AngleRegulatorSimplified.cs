﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_AngleRegulatorSimplified : C_Regulation {
    public Transform m_angleAnchor;
    public float m_angleDelta;
    public int axis;

    public override void M_ProcessSignal(float signal)
    {
        if (axis == 1) m_angleDelta = -M_Angle(m_angleAnchor.localEulerAngles.x, signal);
        else if (axis == 2) m_angleDelta = -M_Angle(m_angleAnchor.localEulerAngles.y, signal);
        else if (axis == 3) m_angleDelta = -M_Angle(m_angleAnchor.localEulerAngles.z, signal);
        
        if (m_angleDelta > 0.5) m_outputSignal = m_powerSupply.M_GiveVoltage();
        else if (m_angleDelta < -0.5) m_outputSignal = -1 * m_powerSupply.M_GiveVoltage();
        else m_outputSignal = 0;
        

        base.M_ProcessSignal(signal);
    }
    public float M_Angle(float val1, float val2)
    {
        val1 = val1 - val2;
        if (val1 > 180)
            val1 = val1 - 360;
        if (val1 < -180)
            val1 = val1 + 360;
        return val1;
    }
}
