﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_RegulatorSimplified : C_Regulation {
    public override void M_ProcessSignal(float signal)
    {
        m_outputSignal = m_powerSupply.M_GiveVoltage() * signal;
        base.M_ProcessSignal(signal);
    }
}
