﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Regulation : MonoBehaviour {
    public C_PowerSupply m_powerSupply;
    public C_Engine m_engine;

    public float m_outputSignal;
    
    virtual public void M_ProcessSignal(float signal)
    {
        m_engine.M_ProcessSignal(m_outputSignal);
    }
    public void M_Init()
    {
        m_engine = transform.GetComponent<C_Engine>();
        m_engine.M_Init();
    }
}
