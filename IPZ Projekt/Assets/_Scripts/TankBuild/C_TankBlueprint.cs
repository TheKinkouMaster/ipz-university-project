﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_TankBlueprint {
    public string m_modelname;
    public string m_model;

    public float engineMaxPower;
    public float engineMaxRPM;

    public float EngineShaftLength;
    public float EngineShaftRadius;
    public float EngineMaxTorque;

    public float WheelShaftLength;
    public float WheelShaftRadius;

    public float smallerGearThickness;
    public float largerGearThickness;
    public float smallerGearRadius;
    public float largerGearRadius;
    public float ratio;
    public float powerLosage;

    public float wheelThickness;
    public float wheelRadius;

    public float trackLength;
    public float trackWidth;
    public float trackThickness;
    public float trackDistance;

    public float tankMass;
    public float k1;
    public float k2;

    public void M_Create()
    {
        m_modelname = new string("modelnames".ToCharArray());
        m_model = new string("models".ToCharArray());
        engineMaxPower = 0;
        engineMaxRPM = 0;
        EngineShaftLength = 0;
        EngineShaftRadius = 0;
        WheelShaftLength = 0;
        wheelRadius = 0;
        smallerGearThickness = 0;
        smallerGearRadius = 0;
        largerGearRadius = 0;
        largerGearThickness = 0;
        wheelThickness = 0;
        wheelRadius = 0;
        trackLength = 0;
        trackWidth = 0;
        trackThickness = 0;
        ratio = 0;
        powerLosage = 0;
        trackDistance = 0;
        tankMass = 0;
        EngineMaxTorque = 0;
        k1 = 0;
        k2 = 0;
    }
}
