﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Physics_TankNetwork : MonoBehaviour {
    public GameObject m_uiAnchor;
    public C_TankBlueprint m_blueprint;
    public C_TankNetwork m_LEGACYTankNetwork;
    public struct TrackNetwork
    {
        public Physics_Engine engine;
        public Physics_Shaft engineShaft;
        public Physics_Gear gear1;
        public Physics_Gear gear2;
        public Physics_Shaft trackShaft;
        public Physics_Wheel wheel;
        public Physics_Track track;
    }
    public struct DataSeries
    {
        public float x;
        public float y;
    }
    public TrackNetwork m_leftTrack;
    public TrackNetwork m_rightTrack;
    public Rigidbody m_rb;
    public float m_tankMass;
    public struct EnergyBufor
    {
        public float inertia;
        public float ratio;
        public float angularSpeed;
    }
    public void M_CreateTank(C_TankBlueprint blueprint, Transform UIAnchor, C_Camera camera)
    {
        m_blueprint = blueprint;
        m_uiAnchor = UIAnchor.gameObject;
        
        if(blueprint.m_model == "Legacy")
        {
            m_LEGACYTankNetwork = GetComponent<C_TankNetwork>();
            m_LEGACYTankNetwork.m_controller.m_mode = C_Controller.Mode.manual;
            m_LEGACYTankNetwork.m_controller.m_camera = camera;
        }
        M_CreateDisplay();
        if (blueprint.m_model != "Legacy")
        {
            m_leftTrack = new TrackNetwork();
            m_rightTrack = new TrackNetwork();
            m_leftTrack = M_CreateTrack(blueprint);
            m_rightTrack = M_CreateTrack(blueprint);
            m_rb = gameObject.AddComponent<Rigidbody>();
            m_rb.mass = 1;
            m_rb.useGravity = true;
        }
        M_UpdateDisplay();
    }
    public TrackNetwork M_CreateTrack(C_TankBlueprint blueprint)
    {
        TrackNetwork track = new TrackNetwork();
        track.engine = transform.gameObject.AddComponent<Physics_Engine>();
        track.engineShaft = transform.gameObject.AddComponent<Physics_Shaft>();
        track.gear1 = transform.gameObject.AddComponent<Physics_Gear>();
        track.gear2 = transform.gameObject.AddComponent<Physics_Gear>();
        track.trackShaft = transform.gameObject.AddComponent<Physics_Shaft>();
        track.wheel = transform.gameObject.AddComponent<Physics_Wheel>();
        track.track = transform.gameObject.AddComponent<Physics_Track>();

        track.engine.m_maxPower = blueprint.engineMaxPower;
        track.engine.m_maxRPM = blueprint.engineMaxRPM;
        track.engine.m_currentSetup = 0;
        track.engine.m_maxTorque = blueprint.EngineMaxTorque;

        track.engineShaft.m_angularVelocity = 0;
        track.engineShaft.m_mass = blueprint.EngineShaftLength / 1000 * 3.1415f * Mathf.Pow(blueprint.EngineShaftRadius/1000,2) * 8.05f;
        track.engineShaft.m_inertia = 0.5f * track.engineShaft.m_mass * Mathf.Pow(blueprint.EngineShaftRadius / 1000, 2);

        track.gear1.m_angularVelocity = 0;
        track.gear1.m_size = blueprint.smallerGearRadius / 1000;
        track.gear1.m_mass = blueprint.smallerGearThickness / 1000 * 3.1415f * Mathf.Pow(blueprint.smallerGearRadius / 1000, 2) * 8.05f; 
        track.gear1.m_inertia = 0.5f * track.gear1.m_mass * Mathf.Pow(track.gear1.m_size,2);

        track.gear2.m_angularVelocity = 0;
        track.gear2.m_size = blueprint.largerGearRadius / 1000;
        track.gear2.m_mass = blueprint.largerGearThickness / 1000 * 3.1415f * Mathf.Pow(blueprint.largerGearRadius / 1000, 2) * 8.05f;
        track.gear2.m_inertia = 0.5f * track.gear2.m_mass * Mathf.Pow(track.gear2.m_size, 2);

        track.gear1.m_ratio = blueprint.ratio;
        track.gear2.m_ratio = blueprint.ratio;
        track.gear1.m_powerLosage = blueprint.powerLosage;
        track.gear2.m_powerLosage = blueprint.powerLosage;

        track.trackShaft.m_angularVelocity = 0;
        track.trackShaft.m_mass = blueprint.WheelShaftLength / 1000 * 3.1415f * Mathf.Pow(blueprint.WheelShaftRadius / 1000, 2) * 8.05f;
        track.trackShaft.m_inertia = 0.5f * track.trackShaft.m_mass * Mathf.Pow(blueprint.WheelShaftRadius / 1000, 2);

        track.wheel.m_angularVelocity = 0;
        track.wheel.m_mass = blueprint.wheelThickness / 1000 * 3.1415f * Mathf.Pow(blueprint.wheelRadius / 1000, 2) * 0.15f;
        track.wheel.m_inertia = 0.5f * track.wheel.m_mass * Mathf.Pow(blueprint.wheelRadius / 1000, 2);
        track.wheel.m_radius = blueprint.wheelRadius;

        track.track.m_angularVelocity = 0;
        track.track.m_mass = blueprint.trackLength / 1000 * blueprint.trackThickness / 1000 * blueprint.trackWidth / 1000 * 4.05f;
        track.track.m_inertia = 0.5f * track.track.m_mass * Mathf.Pow(blueprint.trackLength / 1000 / 15, 2) * 0.1f;
        track.track.m_trackDistance = blueprint.trackDistance;

        m_tankMass = blueprint.tankMass;
        m_k1 = blueprint.k1;
        m_k2 = blueprint.k2;
        m_frictionRatio = 1f;
        return track;
    }
    public Text m_TLeftEngineUsage;
    public Text m_TRightEngineUsage;
    public Text m_TLeftEngineSpeed;
    public Text m_TRightEngineSpeed;
    public Text m_TLeftWheelSpeed;
    public Text m_TRightWheelSpeed;
    public Text m_TLeftEngineWorking;
    public Text m_TRightEngineWorking;
    public Text m_TEnergyLost;
    public Text m_TEnergyGot;
    public Text m_TTotalEnergy;
    public GameObject m_RecordObj;
    public Text m_ModeObj;
    public void M_CreateDisplay()
    {
        List<Text> list = new List<Text>();
        list.AddRange(m_uiAnchor.GetComponentsInChildren<Text>());
        foreach(Text obj in list)
        {
            if (obj.gameObject.name == "LEP") m_TLeftEngineUsage = obj;
            else if (obj.gameObject.name == "REP") m_TRightEngineUsage = obj;
            else if (obj.gameObject.name == "LES") m_TLeftEngineSpeed = obj;
            else if (obj.gameObject.name == "RES") m_TRightEngineSpeed = obj;
            else if (obj.gameObject.name == "LWS") m_TLeftWheelSpeed = obj;
            else if (obj.gameObject.name == "RWS") m_TRightWheelSpeed = obj;
            else if (obj.gameObject.name == "LEW") m_TLeftEngineWorking = obj;
            else if (obj.gameObject.name == "REW") m_TRightEngineWorking = obj;
            else if (obj.gameObject.name == "EL") m_TEnergyLost = obj;
            else if (obj.gameObject.name == "EG") m_TEnergyGot = obj;
            else if (obj.gameObject.name == "TE") m_TTotalEnergy = obj;
        }
        List<Button> list2 = new List<Button>();
        list2.AddRange(m_uiAnchor.GetComponentsInChildren<Button>(true));
        foreach(Button obj in list2)
        {
            if(obj.gameObject.name == "REC")
            {
                m_RecordObj = obj.gameObject;
                m_RecordObj.SetActive(false);
            }
            if (obj.gameObject.name == "MODE")
                m_ModeObj = obj.GetComponentInChildren<Text>(true);
        }
    }
    public void M_UpdateDisplay()
    {
        if(m_blueprint.m_model == "Legacy")
        {
            m_TLeftEngineUsage.text = "not available on Legacy"; 
            m_TRightEngineUsage.text = "not available on Legacy";
            m_TLeftEngineSpeed.text = "not available on Legacy";
            m_TRightEngineSpeed.text = "not available on Legacy";
            m_TLeftWheelSpeed.text = "not available on Legacy";
            m_TRightWheelSpeed.text = "not available on Legacy";
            m_TLeftEngineWorking.text = "not available on Legacy";
            m_TRightEngineWorking.text = "not available on Legacy";
            m_TEnergyLost.text = "not available on Legacy";
            m_TEnergyGot.text = "not available on Legacy";
            m_TTotalEnergy.text = "not available on Legacy";
            m_ModeObj.text = m_LEGACYTankNetwork.m_controller.m_mode.ToString();
            return;
        }
        m_TLeftEngineUsage.text = (m_leftTrack.engine.m_currentSetup*100).ToString() + "%";
        m_TRightEngineUsage.text = (m_rightTrack.engine.m_currentSetup*100).ToString() + "%";
        m_TLeftEngineSpeed.text = m_leftTrack.engineShaft.m_angularVelocity.ToString();
        m_TRightEngineSpeed.text = m_rightTrack.engineShaft.m_angularVelocity.ToString();
        m_TLeftWheelSpeed.text = m_leftTrack.wheel.m_angularVelocity.ToString();
        m_TRightWheelSpeed.text = m_rightTrack.wheel.m_angularVelocity.ToString();
        m_TLeftEngineWorking.text = m_leftTrack.engine.m_working.ToString();
        m_TRightEngineWorking.text = m_rightTrack.engine.m_working.ToString();
        m_TEnergyLost.text = m_energyLost.ToString();
        m_TEnergyGot.text = m_energyGot.ToString();
        m_TTotalEnergy.text = m_totalEnergy.ToString();
    }
    private float changeSpeed = 0.1f;
    public void M_ReadKeys()
    {
        float value = changeSpeed;
        if (m_blueprint.m_model == "Legacy")
        {
            m_LEGACYTankNetwork.M_Update();
            M_UpdateDisplay();
            
            return;
        }
        if (Input.GetKey(KeyCode.LeftShift)) value = value * 10;
        if (Input.GetKey(KeyCode.Q)) m_leftTrack.engine.M_ChangePower(value);
        if (Input.GetKey(KeyCode.A)) m_leftTrack.engine.M_ChangePower(-1*value);
        if (Input.GetKey(KeyCode.R)) m_rightTrack.engine.M_ChangePower(value);
        if (Input.GetKey(KeyCode.F)) m_rightTrack.engine.M_ChangePower(-1*value);
        if (Input.GetKey(KeyCode.W)) m_leftTrack.engine.m_working = true;
        else m_leftTrack.engine.m_working = false;
        if (Input.GetKey(KeyCode.E)) m_rightTrack.engine.m_working = true;
        else m_rightTrack.engine.m_working = false;
        if (Input.GetKeyDown(KeyCode.B)) M_Record();
        M_UpdateDisplay();
        if(m_blueprint.m_model != "Legacy") M_Calculate();
    }
    public void M_Calculate()
    {
        m_energyGot = 0;
        m_energyLost = 0;
        m_totalEnergy = 0;
        float lta = M_CalculatePhysics(m_leftTrack);
        float rta = M_CalculatePhysics(m_rightTrack);
        
        m_rb.AddForce(transform.forward * lta / m_tankMass);
        m_rb.AddForce(transform.forward * rta / m_tankMass);
        m_rb.AddTorque(transform.up * lta * m_leftTrack.track.m_trackDistance / (2 * m_tankMass));
        m_rb.AddTorque(transform.up * -1 * rta * m_leftTrack.track.m_trackDistance / (2 * m_tankMass));
        
        M_UpdateVelocities(m_leftTrack, Mathf.Sin(Vector3.Angle(transform.forward, m_rb.velocity)*3.1415f/180)*m_rb.velocity.magnitude * 10);
        M_UpdateVelocities(m_rightTrack, Mathf.Sin(Vector3.Angle(transform.forward, m_rb.velocity) * 3.1415f / 180) * m_rb.velocity.magnitude *10);

        if(m_recording == true)
        {
            DataSeries data = new DataSeries();
            if (m_DSEnergyLost.Count == 0) data.x = 0;
            else data.x = m_DSEnergyLost[m_DSEnergyLost.Count - 1].x + Time.deltaTime;
            data.y = m_energyLost;
            m_DSEnergyLost.Add(data);
        }

        M_UpdateDisplay();
    }
    public float m_energyGot;
    public float m_energyLost;
    public float m_totalEnergy;
    public void M_UpdateVelocities(TrackNetwork track, float velocity)
    {
        track.track.m_angularVelocity = (velocity * 1000) / track.wheel.m_radius;
        track.wheel.m_angularVelocity = (velocity * 1000) / track.wheel.m_radius;
        track.engineShaft.m_angularVelocity = (velocity * 1000) / track.wheel.m_radius;
        track.gear2.m_angularVelocity = (velocity * 1000) / track.wheel.m_radius;
        track.gear1.m_angularVelocity = (velocity * 1000 * track.gear1.m_ratio) / track.wheel.m_radius;
        track.engineShaft.m_angularVelocity = (velocity * 1000 * track.gear1.m_ratio) / track.wheel.m_radius;
    }
    public float M_CalculatePhysics(TrackNetwork track)
    {
        float torque = 0;
        if (track.engine.m_working == true)
        {
            torque = track.engine.m_maxPower;
            torque = torque * track.engine.m_currentSetup;
            torque = torque * (1 - track.gear1.m_powerLosage);
            torque = torque * track.gear1.m_ratio;
        }
        m_energyGot += torque;

        float counterTorque = 0;
        counterTorque += track.engineShaft.m_angularVelocity * m_k1;
        counterTorque += Mathf.Sign(track.engineShaft.m_angularVelocity) * m_k2;
        m_energyLost += counterTorque;

        float totalTorque = torque - counterTorque;
        m_totalEnergy += totalTorque;

        float force = totalTorque * track.wheel.m_radius / 1000;

        float acceleration = force / m_tankMass;

        float dVelocity = acceleration * Time.deltaTime;

        return force;
    }
    public void M_CalculateOpositeTorque(TrackNetwork track, float momentum)
    {
        float changeOfAngularSpeed = 0;
        float inertia = 0;
        inertia = track.track.m_inertia +
            track.wheel.m_inertia +
            track.trackShaft.m_inertia +
            track.gear2.m_inertia +
            track.gear1.m_inertia * track.gear1.m_ratio +
            track.engineShaft.m_inertia * track.gear2.m_ratio;
        changeOfAngularSpeed = momentum * Time.deltaTime / inertia;
        track.track.m_angularVelocity += changeOfAngularSpeed;
        track.wheel.m_angularVelocity += changeOfAngularSpeed;
        track.trackShaft.m_angularVelocity += changeOfAngularSpeed;
        track.gear1.m_angularVelocity += changeOfAngularSpeed;
        track.gear2.m_angularVelocity += changeOfAngularSpeed;
        track.engineShaft.m_angularVelocity += changeOfAngularSpeed;
    }
    public void M_CalculateSpeedChange(TrackNetwork track, float momentum)
    {
        float changeOfAngularSpeed = 0;
        float inertia = 0;
        inertia = track.track.m_inertia +
            track.wheel.m_inertia +
            track.trackShaft.m_inertia +
            track.gear2.m_inertia +
            track.gear1.m_inertia * track.gear1.m_ratio +
            track.engineShaft.m_inertia * track.gear2.m_ratio;
        changeOfAngularSpeed = momentum * Time.deltaTime / inertia;
        track.track.m_angularVelocity += changeOfAngularSpeed;
        track.wheel.m_angularVelocity += changeOfAngularSpeed;
        track.trackShaft.m_angularVelocity += changeOfAngularSpeed;
        track.gear1.m_angularVelocity += changeOfAngularSpeed;
        track.gear2.m_angularVelocity += changeOfAngularSpeed;
        track.engineShaft.m_angularVelocity += changeOfAngularSpeed;
    }
    public float M_CalculateMomentum(TrackNetwork track)
    {
        float newMomentum = M_CalculateGivenMomentum(track);
        m_energyGot += newMomentum;
        float counterMomentum = M_CalculateCounterMomentum(track);
        counterMomentum = counterMomentum * Mathf.Sign(track.wheel.m_angularVelocity);
        m_energyLost += counterMomentum;
        float effectiveMomentum = newMomentum + counterMomentum;
        m_totalEnergy += effectiveMomentum;

        return effectiveMomentum;
    }
    public float M_CalculateGivenMomentum(TrackNetwork track)
    {
        float engineMomentum = 0;
        if (track.engine.m_working == true) engineMomentum = track.engine.m_maxTorque * track.engine.m_currentSetup;
        else engineMomentum = 0;
        float momentumAfterGears = (engineMomentum * (1 - track.gear1.m_powerLosage)) * track.gear1.m_ratio;
        return momentumAfterGears;
    }
    public float M_CalculateCounterMomentum(TrackNetwork track)
    {
        float retval = 0;
        retval += M_CalculateCounterMomentumInside(track);
        retval += M_CalculateCounterMomentumOutside(track);
        return retval;
    }
    public float M_CalculateCounterMomentumInside(TrackNetwork track)
    {
        float retval = 0;

        retval += m_k1 * track.engineShaft.m_angularVelocity;
        if (track.engineShaft.m_angularVelocity != 0) retval += m_k2 * Mathf.Sign(track.engineShaft.m_angularVelocity);

        return retval;
    }
    public float M_CalculateCounterMomentumOutside(TrackNetwork track)
    {
        float weight = m_tankMass * 0.81f;
        float friction = m_frictionRatio * weight;
        float momentum = friction * track.wheel.m_radius / 1000;
        return momentum;
    }
    public float M_CalculateNewEnergy(TrackNetwork track)
    {
        float energyGot = track.engine.m_maxPower * track.engine.m_currentSetup * Time.deltaTime;
        energyGot = energyGot * track.gear1.m_powerLosage;
        return energyGot;
    }
    public void M_RecalculateAngularVelocity(TrackNetwork track, float energy)
    {
        float retval = 0;
        retval = track.engineShaft.m_inertia + track.gear1.m_inertia +
            (track.gear2.m_inertia / Mathf.Pow(track.gear2.m_ratio, 2)) +
            (track.trackShaft.m_inertia / Mathf.Pow(track.gear2.m_ratio, 2)) +
            (track.wheel.m_inertia / Mathf.Pow(track.gear2.m_ratio, 2)) +
            (track.track.m_inertia / Mathf.Pow(track.gear2.m_ratio, 2));
        retval = retval / 2;
        retval = retval / energy;
        float sign;
        retval = 1 / retval;
        sign = Mathf.Sign(retval);
        if (retval >= 0) retval = Mathf.Sqrt(retval);
        else retval = Mathf.Sqrt(retval * -1);

        track.engineShaft.m_angularVelocity = (retval * sign);
        track.gear1.m_angularVelocity = (retval * sign);

        track.gear2.m_angularVelocity = (retval * sign) / track.gear2.m_ratio;
        track.trackShaft.m_angularVelocity = (retval * sign) / track.gear2.m_ratio;
        track.wheel.m_angularVelocity = (retval * sign) / track.gear2.m_ratio;
        track.track.m_angularVelocity = (retval * sign) / track.gear2.m_ratio;
    }
    public float M_CalculatedLoses(TrackNetwork track)
    {
        float losesInside = M_CalculateLosesInside(track);
        float losesOutside = M_CalculatureLosesOutside(track);
        return losesInside + losesOutside;
    }
    public float M_CalculateLosesInside(TrackNetwork track)
    {
        float retval = 0;

        retval += 0.005f * track.engineShaft.m_angularVelocity;
        if(track.engineShaft.m_angularVelocity!=0) retval += 0.001f * Mathf.Sign(track.engineShaft.m_angularVelocity);

        return retval;
    }
    public float m_k1;
    public float m_k2;
    public float m_frictionRatio;
    public float M_CalculatureLosesOutside(TrackNetwork track)
    {
        float frictionForce = m_tankMass * 9.81f * 1;
        float momentum = frictionForce * track.wheel.m_radius / 1000;
        return momentum;
    }
    public float M_AquireEnergy(TrackNetwork track)
    {
        float retval = 0;
        retval += track.engine.m_angularVelocity * track.engine.m_angularVelocity * track.engine.m_inertia / 2;
        retval += track.engineShaft.m_angularVelocity * track.engineShaft.m_angularVelocity * track.engineShaft.m_inertia / 2;
        retval += track.gear1.m_angularVelocity * track.gear1.m_angularVelocity * track.gear1.m_inertia / 2;
        retval += track.gear2.m_angularVelocity * track.gear2.m_angularVelocity * track.gear2.m_inertia / 2;
        retval += track.trackShaft.m_angularVelocity * track.trackShaft.m_angularVelocity * track.trackShaft.m_inertia / 2;
        retval += track.wheel.m_angularVelocity * track.wheel.m_angularVelocity * track.wheel.m_inertia / 2;
        retval += track.track.m_angularVelocity * track.track.m_angularVelocity * track.track.m_inertia / 2;
        if (track.engineShaft.m_angularVelocity < 0) retval = retval * -1;
        return retval;
    }
    public C_IO m_io;
    public bool m_recording = false;
    public List<DataSeries> m_DSEnergyLost;
    public void M_Record()
    {
        if(m_recording == false)
        {
            m_recording = true;
            m_RecordObj.SetActive(true);
            m_DSEnergyLost = new List<DataSeries>();
            return;
        }
        else if(m_recording == true)
        {
            m_recording = false;
            m_RecordObj.SetActive(false);
            if (m_DSEnergyLost != null)
                if (m_DSEnergyLost.Count > 10)
                {
                    m_io.M_SaveToCSV(m_DSEnergyLost, "Moment oporowy");
                }
                
            
        }
    }
}
