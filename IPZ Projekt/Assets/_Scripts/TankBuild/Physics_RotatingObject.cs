﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Physics_RotatingObject : Physics_PhysicalObject {
    public float m_angularVelocity;
    public float m_inertia;
}
