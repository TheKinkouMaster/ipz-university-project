﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Track : MonoBehaviour {
    public List<C_Gear> m_gears;

    public float m_ETorque;
    public float m_OTorque;
    public float m_DTorque;
    public float m_TrackDistance;

    public float M_DForce;

    public float m_trackRadius;

    public void M_Init()
    {
        m_gears.AddRange(GetComponentsInChildren<C_Gear>());
        m_TrackDistance = transform.localPosition.x;
    }
    public void M_Process()
    {
        m_ETorque = 0;
        foreach (C_Gear obj in m_gears)
            m_ETorque += obj.m_torqueOn;

        m_DTorque = m_ETorque;
        M_DForce = m_DTorque / m_trackRadius;
        //m_angularVelocity += m_angularAcceleration / Time.deltaTime;
    }
    public float M_Sign(float value)
    {
        if (value > 0) return 1;
        if (value < 0) return -1;
        return 0;
    }
}
