﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_TankPhysicsModule : MonoBehaviour
{
    public List<C_Track> m_Tracks;
    public C_Track m_TowerRot;
    public C_Track m_CannonRot;
    public Rigidbody m_rb;
    public Transform m_rbT;
    public Transform m_rbC;

    public float m_totalForce;
    public float m_totalTorque;

    public float m_pushingForce;
    public float m_rotatingTorque;

    public float m_OForce;
    public float m_OTorque;
    public float m_friction;
    public float m_angularFriction;

    public float m_mass;

    public void M_Process()
    {
        m_totalForce = 0;
        m_totalTorque = 0;
        //liczenie sil i momentow
        foreach (C_Track obj in m_Tracks)
        {
            m_totalForce += obj.M_DForce;
            m_totalTorque += obj.M_DForce * obj.m_TrackDistance * -1;
        }

        //momenty oporowe
        m_OForce = m_mass * 9.8f * m_friction;
        m_OTorque = m_mass * 9.8f * m_angularFriction * m_Tracks[0].m_TrackDistance;
        
        //dodawanie tego do fizyki
        m_rb.AddForce(transform.forward * m_totalForce);
        int tmp = 0;
        foreach (C_Track obj in m_Tracks)
            if (obj.M_DForce != 0) tmp++;
        if(tmp != m_Tracks.Count)
            m_rb.AddForce(m_rb.velocity.normalized * m_OForce * -1);
        m_rb.AddTorque(0, m_totalTorque * 2, 0);
        m_rb.AddTorque(0, m_OTorque * m_rb.angularVelocity.y * 20, 0);

        //wiezyczka
        float rot = (m_TowerRot.m_DTorque / 5000000) / (Time.deltaTime * Time.deltaTime);
        m_rbT.localEulerAngles = m_rbT.localEulerAngles + new Vector3(0,rot,0);
        rot = (m_CannonRot.m_DTorque / 5000000) / (Time.deltaTime * Time.deltaTime);
        m_rbC.localEulerAngles = m_rbC.localEulerAngles + new Vector3(rot, 0, 0);
    }
    public void M_Init()
    {
        m_Tracks.Add(transform.Find("LeftTrack").GetComponent<C_Track>());
        m_Tracks.Add(transform.Find("RightTrack").GetComponent<C_Track>());
        m_TowerRot = transform.Find("Tower").GetComponent<C_Track>();
        m_CannonRot = transform.Find("Cannon").GetComponent<C_Track>();
        m_rb = GetComponent<Rigidbody>();
        m_mass = m_rb.mass;
    }
    public float M_Sign(float value)
    {
        if (value > 0) return 1;
        if (value < 0) return -1;
        return 0;
    }

}
