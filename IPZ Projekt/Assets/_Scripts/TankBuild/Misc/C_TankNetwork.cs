﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_TankNetwork : MonoBehaviour {
    public C_Controller m_controller;

    public Transform m_leftTrackAnchor;
    public List<C_Regulation> m_leftTrackRegulators;
    public List<C_Engine> m_leftTrackEngines;

    public Transform m_rightTrackAnchor;
    public List<C_Regulation> m_rightTrackRegulators;
    public List<C_Engine> m_rightTrackEngines;

    public C_Regulation m_TowerReg;
    public C_Engine m_TowerEng;
    public C_Track m_towerTrack;

    public C_Regulation m_CannonReg;
    public C_Engine m_CannonEng;
    public C_Track m_cannonTrack;

    public List<C_Track> m_tracks;


    public C_TankPhysicsModule m_physicsModule;

    public C_PowerSupply m_powerSupply;

    public void Awake()
    {
        M_Init();
    }
    public void M_Update()
    {
        m_controller.M_Process();
        foreach (C_Track obj in m_tracks)
            obj.M_Process();
        m_cannonTrack.M_Process();
        m_towerTrack.M_Process();
        m_physicsModule.M_Process();
    }
    public void FixedUpdate()
    {
    }
    public void M_Init()
    {
        //czyszczenie
        m_leftTrackAnchor = null;
        m_rightTrackAnchor = null;
        m_controller = null;
        m_leftTrackEngines = new List<C_Engine>();
        m_rightTrackEngines = new List<C_Engine>();
        m_leftTrackRegulators = new List<C_Regulation>();
        m_rightTrackRegulators = new List<C_Regulation>();
        m_tracks = new List<C_Track>();
        m_powerSupply = null;
        m_physicsModule = null;
        m_TowerEng = null;
        m_TowerReg = null;
        m_CannonEng = null;
        m_CannonReg = null;
        m_cannonTrack = null;
        m_towerTrack = null; 

        //przypisywanie lokalnych danych
        m_controller = GetComponentInChildren<C_Controller>();
        m_powerSupply = GetComponentInChildren<C_PowerSupply>();
        m_tracks.AddRange(GetComponentsInChildren<C_Track>());

        m_physicsModule = GetComponent<C_TankPhysicsModule>();

        m_leftTrackAnchor = transform.Find("LeftTrack");
        m_rightTrackAnchor = transform.Find("RightTrack");

        m_TowerReg = transform.Find("Tower").GetComponentInChildren<C_Regulation>();
        m_TowerReg.M_Init();
        m_TowerReg.m_powerSupply = m_powerSupply;
        m_TowerEng = m_TowerReg.m_engine;

        m_CannonReg = transform.Find("Cannon").GetComponentInChildren<C_Regulation>();
        m_CannonReg.M_Init();
        m_CannonReg.m_powerSupply = m_powerSupply;
        m_CannonEng = m_CannonReg.m_engine;

        m_cannonTrack = transform.Find("Cannon").GetComponent<C_Track>();
        m_towerTrack = transform.Find("Tower").GetComponent<C_Track>();

        m_cannonTrack.M_Init();
        m_towerTrack.M_Init();

        m_leftTrackRegulators.AddRange(m_leftTrackAnchor.GetComponentsInChildren<C_Regulation>());
        m_rightTrackRegulators.AddRange(m_rightTrackAnchor.GetComponentsInChildren<C_Regulation>());

        foreach (C_Regulation obj in m_leftTrackRegulators)
        {
            obj.M_Init();
            m_leftTrackEngines.Add(obj.m_engine);
            obj.m_powerSupply = m_powerSupply;
        }
        foreach (C_Regulation obj in m_rightTrackRegulators)
        {
            obj.M_Init();
            m_rightTrackEngines.Add(obj.m_engine);
            obj.m_powerSupply = m_powerSupply;
        }
        foreach (C_Track obj in m_tracks)
        {
            obj.M_Init();
        }
        m_physicsModule.M_Init();

        //wysyłanie danych lokalnych
        m_controller.M_AttachComponents(m_leftTrackRegulators, m_rightTrackRegulators);
        m_controller.m_cannonReg = m_CannonReg;
        m_controller.m_towerReg = m_TowerReg;
    }
}
