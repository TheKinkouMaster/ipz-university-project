﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Gear : MonoBehaviour {
    public float m_gearRatio;
    public float m_efficiency;
    public float m_torqueOn;

    public void M_ProcessSignal(float torque)
    {
        m_torqueOn = torque * m_gearRatio * m_efficiency;
    }
}
