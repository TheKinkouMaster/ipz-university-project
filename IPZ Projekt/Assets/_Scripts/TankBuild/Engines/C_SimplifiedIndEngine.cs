﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_SimplifiedIndEngine : C_Engine {
    public float m_power;
    public float m_rpm;

    public override void M_ProcessSignal(float signal)
    {
        m_outputTorque = (signal / m_maxVoltage) * m_power;
        m_outputTorque = (m_outputTorque*60) / (m_rpm*2f*3.1415f);
        m_gear.M_ProcessSignal(m_outputTorque);
    }
}
