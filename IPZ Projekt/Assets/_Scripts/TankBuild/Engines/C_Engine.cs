﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Engine : C_PhysicalObject {
    public C_PowerSupply m_powerSupply;

    public float m_maxForwardSpeed;
    public float m_maxBackwardSpeed;

    public float m_maxVoltage;

    public float m_outputTorque;

    public C_Gear m_gear;

    virtual public void M_ProcessSignal(float signal)
    {
        m_gear.M_ProcessSignal(m_outputTorque);
    }

    public void M_Init()
    {
        m_gear = GetComponent<C_Gear>();
    }
}
