﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Physics_TankInfo : MonoBehaviour {
    public Transform m_turret;
    public Transform m_cannon;
    public Transform m_cameraPosition;
    public Transform m_cameraTPSPosition;
    public bool m_legacy;
}
