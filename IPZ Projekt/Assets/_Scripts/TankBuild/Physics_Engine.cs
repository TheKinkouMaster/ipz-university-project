﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Physics_Engine : Physics_RotatingObject {
    public float m_maxPower;
    public float m_maxRPM;
    public float m_currentSetup;
    public float m_maxTorque;
    public bool m_working;
    public void M_ChangePower(float value)
    {
        m_currentSetup +=  (value) * Time.deltaTime;
        if (m_currentSetup > 1) m_currentSetup = 1;
        if (m_currentSetup < -1) m_currentSetup = -1;
    }
}
