﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_RotatingObject : C_PhysicalObject {
    public float m_angularSpeed;
    public float m_height;
    public float m_radius;

    virtual public void M_Init()
    {
    }
    virtual public void M_AddTorque(float torque)
    {
        m_angularSpeed += (torque / m_angularSpeed) * Time.deltaTime;
    }
}
