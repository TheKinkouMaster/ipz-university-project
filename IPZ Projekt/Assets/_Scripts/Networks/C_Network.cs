﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class C_Network : MonoBehaviour {
    public C_Neuron m_thirst;
    public C_Neuron m_hunger;
    public C_Neuron m_sleepness;

    public List<C_Neuron> m_neurons;

    public C_Neuron m_sleep;
    public C_Neuron m_eat;
    public C_Neuron m_drink;
    public C_Neuron m_idle;

    public void M_Input(float t, float h, float s)
    {
        m_thirst.m_value = t;
        m_hunger.m_value = h;
        m_sleepness.m_value = s;
    }
    public void M_Process()
    {
        foreach (C_Neuron obj in m_neurons)
            obj.M_CalculateValue();
        m_sleep.M_CalculateValue();
        m_eat.M_CalculateValue();
        m_drink.M_CalculateValue();
        m_idle.M_CalculateValue();
    }
    public void M_Learn()
    {

    }
}
