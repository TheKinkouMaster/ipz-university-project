﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Evaluation : MonoBehaviour {
    public float m_doEat;
    public float m_doSleep;
    public float m_doDrink;
    public float m_doIdle;

    public float m_ate;
    public float m_drank;
    public float m_slept;

    public void M_Evaluate()
    {
        m_doEat = m_ate;
        m_doSleep = m_slept;
        m_doDrink = m_drank;
        m_doIdle = 1-((m_doDrink + m_doEat + m_doSleep) / 3);

        m_ate = 0;
        m_slept = 0;
        m_drank = 0;
    }
}
