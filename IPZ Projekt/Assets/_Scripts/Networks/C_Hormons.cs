﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Hormons : MonoBehaviour {
    public float m_dopamine;
    public float m_dopamineChange;

    public float m_dopamineDecay;
    public C_Needs m_needs;

    public int i = 100;

    public void M_CheckValue(float delta)
    {

        m_dopamine = m_dopamine + delta - m_dopamineDecay;
    }
}
