﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Neuron : MonoBehaviour {
    public float m_value;
    public float m_bias;
    public List<C_Link> m_input;
    public List<C_Link> m_output;

    public void M_CalculateValue()
    {
        m_value = 0;
        foreach (C_Link obj in m_input)
            m_value = obj.m_value * obj.m_input.m_value;

        m_value = C_Math.M_Clamp(m_value, -1, 1, 0, 0);
    }
    public void M_Init(int bias, int value)
    {
        m_bias = bias;
        m_value = value;
        m_input = new List<C_Link>();
        m_output = new List<C_Link>();
    }
}
