﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class C_Math {
    public static float M_Clamp(float value, float scalex, float scaley, float offsetx, float offsety)
    {
        float retval = scaley / (1 + Mathf.Pow(2.7182f, scalex*(value + offsetx)));
        retval = retval + offsety;

        return retval;
    }
    public static int M_TryParse(string word)
    {
        int retval;
        Int32.TryParse(word, out retval);
        return retval;
    }
}
