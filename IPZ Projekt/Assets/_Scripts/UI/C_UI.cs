﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class C_UI : MonoBehaviour {
    public bool m_menuON;
    public int m_menuPOS;

    public InputField if_sizex;
    public InputField if_sizey;
    public InputField if_name;

    public C_Camera m_camera;
    public C_Main m_main;
    public C_Data m_data;
    public C_IO m_io;
    public C_Prefab m_prefab;
    public C_Simul m_simul;

    public GameObject m_menuAnchor;
    public GameObject m_simulAnchor;
    public C_UI_Tanks m_UI_Tanks;

    private void Awake()
    {
        m_menuON = true;
        m_menuPOS = 0;
        M_CheckScene();
    }
    
    public void M_ChangeScene(int sceneNumber)
    {
        m_menuPOS = sceneNumber;
        m_camera.M_ChangeScene(sceneNumber);
        if (sceneNumber == 2)
            M_CheckSimulations();
        if (sceneNumber == 4)
            m_UI_Tanks.M_START();
    }
    public void M_CreateNewSimulation()
    {
        int sizex, sizey;
        Int32.TryParse(if_sizex.text, out sizex);
        Int32.TryParse(if_sizey.text, out sizey);

        m_io.M_Save(new L_Data(if_name.text, sizex, sizey));
        //m_data.M_CreateNewSimul(sizex, sizey, if_name.text);
        M_CheckSimulations();
        M_StartSimulation(if_name.text + ".tsf");
    }
    public void M_StartSimulation(string simname)
    {
        m_simul.m_active = true;
        m_simul.M_CreateNewSimul(m_io.M_Load(simname));
        m_camera.m_mainMenuCamera.gameObject.SetActive(false);
        M_CheckScene();
    }
    public Transform m_simulLoadAnchor;
    public List<GameObject> m_simulLoadAnchoritems;
    public int m_simulLoadJump;
    public GameObject m_simulLoadPf;
    public void M_CheckSimulations()
    {
        List<string> m_sims = new List<string>();
        m_sims = m_io.M_CheckSimulations();
        foreach (GameObject obj in m_simulLoadAnchoritems)
            m_prefab.M_AddObject(obj);
        m_simulLoadAnchoritems = new List<GameObject>();
        m_prefab.M_DisableStoredItems();
        for(int i = 0; i<m_sims.Count; i++)
        {
            GameObject obj = m_prefab.M_GiveObject("Pf_Simul");
            obj.SetActive(true);
            obj.transform.SetParent(m_simulLoadAnchor);
            obj.transform.localPosition = new Vector3(0, i * m_simulLoadJump, 0);
            obj.transform.localRotation = new Quaternion(0, 0, 0, transform.localRotation.z);
            obj.transform.localScale = new Vector3(1, 1, 1);
            obj.GetComponent<B_LoadSimul>().M_ChangeText(m_sims[i]);
            m_simulLoadAnchoritems.Add(obj);
        }
    }
    public void M_Back()
    {
        if(m_camera.m_cameraMode == C_Camera.CameraMode.menu)
        {
            if (m_menuPOS == 2)
            {
                M_ChangeScene(3);
                m_menuPOS = 1;
            }
            if (m_menuPOS == 4)
            {
                M_ChangeScene(5);
                m_menuPOS = 1;
            }
            if (m_menuPOS == 6)
            {
                M_ChangeScene(7);
                m_menuPOS = 1;
            }
            if (m_menuPOS == 8)
            {
                M_ChangeScene(9);
                m_menuPOS = 1;
            }
        }
        else if (m_camera.m_cameraMode == C_Camera.CameraMode.fps || m_camera.m_cameraMode == C_Camera.CameraMode.tps)
        {
            m_camera.m_cameraMode = C_Camera.CameraMode.menu;
            m_simul.M_RemoveObjects();
            M_CheckScene();
            m_camera.M_CheckCamera();
        }
    }
    public void M_Exit()
    {
        Application.Quit();
    }
    public void M_Save()
    {
        m_main.M_Save();
        M_Exit();
    }
    public void M_CheckScene()
    {
        if (m_camera.m_cameraMode == C_Camera.CameraMode.fps || m_camera.m_cameraMode == C_Camera.CameraMode.tps)
        {
            m_simulAnchor.SetActive(true);
            m_menuAnchor.SetActive(false);
        }
        else
        {
            m_simulAnchor.SetActive(false);
            m_menuAnchor.SetActive(true);
        }
    }
}
