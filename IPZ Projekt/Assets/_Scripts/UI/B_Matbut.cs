﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class B_Matbut : MonoBehaviour {
    public C_TPS m_tps;

    public void M_CLICK()
    {
        m_tps.M_ChangeMaterial(transform.GetComponentInChildren<Text>().text);
    }
    public void M_SpawnObject()
    {
        m_tps.M_SpawnObject(transform.GetComponentInChildren<Text>().text);
    }
    public void M_INIT(C_TPS tPS)
    {
        m_tps = tPS;
    }
    public C_TPS M_CheckTPS()
    {
        return m_tps;
    }
    public void M_SpawnTank()
    {
        m_tps.M_SpawnTank(transform.GetComponentInChildren<Text>().text);
    }
}
