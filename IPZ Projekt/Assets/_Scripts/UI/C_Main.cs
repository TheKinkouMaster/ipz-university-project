﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Main : MonoBehaviour {
    public C_Camera m_camera;
    public C_IO m_io;
    public C_Data m_data;

    public void M_Save()
    {
        m_data.M_Save();
    }
}
