﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Keyboard : MonoBehaviour {
    private bool m_gameStarted;

    public C_Camera m_mainController;
    public C_UI m_UI;
    public C_Camera m_camera;


    private void Awake()
    {
        m_gameStarted = false;
    }

    private void Update()
    {
        if (m_gameStarted == false)
        {
            if (Input.anyKey)
            {
                m_gameStarted = true;
                m_mainController.M_StartGame();
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                m_UI.M_Back();
            if (Input.GetKeyDown(KeyCode.Tab))
                m_camera.M_ChangeCamera();
            if (Input.GetKeyDown(KeyCode.Y))
                m_camera.M_StartSimulation();
        }
    }
}
