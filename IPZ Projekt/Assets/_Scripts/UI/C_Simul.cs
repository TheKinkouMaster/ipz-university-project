﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class C_Simul : MonoBehaviour {
    public L_Data m_data;
    public GameObject m_plane;
    public C_Prefab m_pf;
    public C_Camera m_camera;
    public C_IO m_io;
    public GameObject m_objectsAnchor;
    public Physics_TankNetwork m_tank;
    public bool m_active;

    public void Awake()
    {

        m_objects = new List<GameObject>();
        m_makingPhotos = false;
    }
    public int m_sampleNumber;
    public List<string> m_targets = new List<string>();
    public void Update()
    {
        if (m_makingPhotos == true)
        {
            if (m_photoPath.Count == 1)
            {
                m_camera.m_TankCamera.gameObject.GetComponentInChildren<Canvas>(true).gameObject.SetActive(true);
                m_makingPhotos = false;
                StreamWriter SW = new StreamWriter(Application.dataPath + "/screenshots" + "/Raport.txt");
                foreach (string obj in m_targets)
                    SW.WriteLine(obj);
                SW.Close();
                m_targets = new List<string>();
            }
            else
            {
                m_tank.gameObject.transform.position = new Vector3(m_photoPath[0].x, 0,m_photoPath[0].y) ;
                m_tank.gameObject.transform.LookAt(new Vector3(m_photoPath[1].x, 0, m_photoPath[1].y));
                m_targets.AddRange(m_camera.M_CaptureScreenshot(Application.dataPath + "/screenshots", m_sampleNumber.ToString()));
                m_sampleNumber++;
                m_photoPath.RemoveAt(0);
            }
        }
        else if (m_camera.m_cameraMode == C_Camera.CameraMode.tank)
        {
            m_tank.M_ReadKeys();
        }
    }
    public void M_DeleteOldSimul()
    {

    }
    public void M_DisableObjectsPhysics(GameObject obj, bool mode)
    {
        if (obj.GetComponent<BoxCollider>() != null) obj.GetComponent<BoxCollider>().enabled = mode;
        foreach (BoxCollider colliders in obj.GetComponentsInChildren<BoxCollider>())
        {
            colliders.enabled = mode;
        }
        if (obj.GetComponent<Rigidbody>() != null) obj.GetComponent<Rigidbody>().useGravity = mode;
        foreach (Rigidbody colliders in obj.GetComponentsInChildren<Rigidbody>())
        {
            colliders.useGravity = mode;
        }
    }
    public void M_CreateNewSimul(L_Data data)
    {
        //aquiring data
        m_data = data;
        //creating world
        m_plane.transform.localScale = new Vector3(m_data.m_size_x, 1, m_data.m_size_y);
        M_ChangePlaneTexture(m_data.m_groundTexture);
        //setting cameras
        m_camera.m_cameraMode = C_Camera.CameraMode.tps;
        m_camera.M_CheckCamera();
        //loading objects
        M_CreateObjects();
    }
    public void M_CreateObjects()
    {
        foreach(L_Data.obj obj in m_data.m_objs)
        {
            GameObject spawnedObj = M_SpawnObject(obj.pfName);
            spawnedObj.transform.position = obj.pos;
            spawnedObj.transform.eulerAngles = obj.rot;
        }
    }
    public void M_RemoveObjects()
    {
        m_camera.M_RetrieveTankCamera();
        m_tank = null;
        foreach(Transform obj in m_objectsAnchor.GetComponentsInChildren<Transform>())
        {
            if (obj != null)
                if (obj.parent == m_objectsAnchor.transform)
                {
                    Destroy(obj.gameObject);
                }
        }
    }
    public void M_ResetSimulation()
    {
        M_RemoveObjects();
        M_CreateNewSimul(m_io.M_Load(m_data.m_name + ".tsf"));
    }
    private List<GameObject> m_objects;
    public GameObject M_GiveTank()
    {
        foreach (Transform obj in m_objectsAnchor.GetComponentsInChildren<Transform>())
            if (obj.gameObject.GetComponent<Physics_TankInfo>() != null)
                return obj.gameObject;
        return null;
    }
    public GameObject M_SpawnObject(string objName)
    {
        GameObject obj;
        if(m_io.M_CheckIfTank(objName) == true) obj = M_SpawnTank(m_io.M_LoadTankModel(objName));
        else obj = m_pf.M_GiveObjects(objName);
        m_objects.Add(obj);
        obj.transform.SetParent(m_objectsAnchor.transform);
        M_DisableObjectsPhysics(obj, true);
        return obj;
    }
    public GameObject M_SpawnTank(C_TankBlueprint objName)
    {
        GameObject obj = m_pf.M_GiveObjects(objName.m_model);
        m_objects.Add(obj);
        obj.transform.SetParent(m_objectsAnchor.transform);
        M_DisableObjectsPhysics(obj, true);
        Physics_TankNetwork obj1 = obj.AddComponent<Physics_TankNetwork>();
        obj1.M_CreateTank(objName,m_camera.m_TankCamera.gameObject.transform, m_camera);
        obj1.m_io = m_io;
        m_tank = obj1;
        return obj;
    }
    public List<Vector2> m_photoPath;
    public bool m_makingPhotos;
    public void M_CreatePhotos(List<Vector3> path, int samples)
    {
        List<Vector2> path2 = new List<Vector2>();
        foreach (Vector3 obj in path)
            path2.Add(new Vector2(obj.x, obj.z));
        m_sampleNumber = 0;
        m_io.M_CreateDirectory("screenshots");
        m_tank.m_LEGACYTankNetwork.m_controller.m_mode = C_Controller.Mode.manual;
        m_camera.M_StartSimulation();
        m_camera.m_TankCamera.gameObject.GetComponentInChildren<Canvas>(true).gameObject.SetActive(false);
        int iloscProbekNaLinie = samples / path.Count;
        for(int j = 0; j < path2.Count-2; j++)
        {
            for(int i = 0; i<iloscProbekNaLinie; i++)
            {
                float ratio = (float)(((float)i) / ((float)iloscProbekNaLinie));
                m_photoPath.Add(Vector2.Lerp(path[j], path2[j + 1], ratio));
            }
        }
        m_makingPhotos = true;
    }
    public void M_ChangePlaneTexture(Material newmat)
    {
        List<Material> tmp = new List<Material>();
        newmat.mainTextureScale = new Vector2(m_plane.transform.localScale.x, m_plane.transform.localScale.z);
        tmp.Add(newmat);
        m_plane.GetComponent<MeshRenderer>().materials = tmp.ToArray();
        m_data.m_groundTexture = newmat.name;
    }
    public void M_ChangePlaneTexture(string newmat)
    {
        Material mat = m_pf.M_GiveMaterial(newmat);
        if (mat != null)
            M_ChangePlaneTexture(mat);
        else
            M_ChangePlaneTexture("Blank");
    }
    public void M_Save()
    {
        m_data.m_objs = new List<L_Data.obj>();
        foreach (Transform obj in m_objectsAnchor.GetComponentsInChildren<Transform>())
        {
            if (obj.parent == m_objectsAnchor.transform)
            {
                if(obj.GetComponent<Physics_TankNetwork>()!=null) m_data.M_AddObj(obj.position, obj.rotation.eulerAngles, obj.GetComponent<Physics_TankNetwork>().m_blueprint.m_modelname);
                else m_data.M_AddObj(obj.position, obj.rotation.eulerAngles, obj.name.Split('(')[0]);
            }
        }
        m_io.M_Save(m_data);
    }
}
