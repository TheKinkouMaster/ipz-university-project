﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class L_Data {
    public string m_name;
    public int m_size_x;
    public int m_size_y;
    public string m_groundTexture;
    public struct obj
    {
        public Vector3 pos;
        public Vector3 rot;
        public string pfName;
    }
    public List<obj> m_objs;
    public L_Data(string name, int sx, int sy)
    {
        m_name = name;
        m_size_x = sx;
        m_size_y = sy;
        m_groundTexture = "Blank";
        m_objs = new List<obj>();
    }
    public L_Data()
    {
        m_objs = new List<obj>();
    }
    public void M_AddObj(Vector3 pos, Vector3 rot, string name)
    {
        obj newobj = new obj();
        newobj.pos = pos;
        newobj.rot = rot;
        newobj.pfName = name;
        m_objs.Add(newobj);
    }
}
