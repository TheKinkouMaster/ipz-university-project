﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class C_IO : MonoBehaviour {
    public C_TankBlueprint M_LoadTankModel(string objname)
    {
        List<C_TankBlueprint> list = M_LoadTankModels();
        foreach (C_TankBlueprint obj in list)
            if (obj.m_modelname == objname) return obj;
        return null;
    }
    public void M_CreateDirectory(string foldername)
    {
        string path = Application.dataPath + "/" + foldername;
        Directory.CreateDirectory(path);
    }
    public List<C_TankBlueprint> M_LoadTankModels()
    {
        List<C_TankBlueprint> retval = new List<C_TankBlueprint>();

        List<string> simulations = new List<string>();
        List<string> simulations2 = new List<string>();
        if (!Directory.Exists(Application.dataPath + "\\BlueprintFolder"))
        {
            Directory.CreateDirectory(Application.dataPath + "\\BlueprintFolder");
        }
        simulations.AddRange(Directory.GetFiles(Application.dataPath + "\\BlueprintFolder"));
        foreach (string obj in simulations)
            if (obj.EndsWith(".tbf")) simulations2.Add(obj.Remove(obj.Length - 4).Remove(0, obj.LastIndexOf('\\') + 1));
        
        foreach (string str in simulations2)
        {
            StreamReader SR = new StreamReader(Application.dataPath + "\\BlueprintFolder\\" + str + ".tbf");

            C_TankBlueprint newBlueprint = new C_TankBlueprint();
            newBlueprint.M_Create();
            string command;
            while(!SR.EndOfStream)
            {
                command = SR.ReadLine();
                if (command == "END")
                    break;
                else if (command == "Modelname")
                    newBlueprint.m_modelname = SR.ReadLine();
                else if (command == "Model")
                    newBlueprint.m_model = SR.ReadLine();
                else if (command == "engineMaxPower")
                    newBlueprint.engineMaxPower = M_ReadNumber(SR);
                else if (command == "EngineMaxRPM")
                    newBlueprint.engineMaxRPM = M_ReadNumber(SR);
                else if (command == "EngineShaftLength")
                    newBlueprint.EngineShaftLength = M_ReadNumber(SR);
                else if (command == "EngineShaftRadius")
                    newBlueprint.EngineShaftRadius = M_ReadNumber(SR);
                else if (command == "WheelShaftLength")
                    newBlueprint.WheelShaftLength = M_ReadNumber(SR);
                else if (command == "WheelShaftRadius")
                    newBlueprint.WheelShaftRadius = M_ReadNumber(SR);
                else if (command == "SmallerGearThickness")
                    newBlueprint.smallerGearThickness = M_ReadNumber(SR);
                else if (command == "LargerGearThickness")
                    newBlueprint.largerGearThickness = M_ReadNumber(SR);
                else if (command == "SmallerGearRadius")
                    newBlueprint.smallerGearRadius = M_ReadNumber(SR);
                else if (command == "LargerGearRadius")
                    newBlueprint.largerGearRadius = M_ReadNumber(SR);
                else if (command == "WheelThickness")
                    newBlueprint.wheelThickness = M_ReadNumber(SR);
                else if (command == "WheelRadius")
                    newBlueprint.wheelRadius = M_ReadNumber(SR);
                else if (command == "TrackLength")
                    newBlueprint.trackLength = M_ReadNumber(SR);
                else if (command == "TrackWidth")
                    newBlueprint.trackWidth = M_ReadNumber(SR);
                else if (command == "TrackThickness")
                    newBlueprint.trackThickness = M_ReadNumber(SR);
                else if (command == "Ratio")
                    newBlueprint.ratio = M_ReadNumber(SR);
                else if (command == "GearPowerLosage")
                    newBlueprint.powerLosage = M_ReadNumber(SR);
                else if (command == "TrackDistance")
                    newBlueprint.trackDistance = M_ReadNumber(SR);
                else if (command == "TankMass")
                    newBlueprint.tankMass = M_ReadNumber(SR);
                else if (command == "EngineMaxTorque")
                    newBlueprint.EngineMaxTorque = M_ReadNumber(SR);
                else if (command == "K1")
                    newBlueprint.k1 = M_ReadNumber(SR);
                else if (command == "K2")
                    newBlueprint.k2 = M_ReadNumber(SR);
                else
                    SR.ReadLine();
            }
            retval.Add(newBlueprint);

            SR.Close();
        }

        return retval;
    }
    public void M_SaveToCSV(List<Physics_TankNetwork.DataSeries> series, string filename)
    {
        string dirname = System.DateTime.Today.ToString("D");
        Debug.Log(Application.dataPath + "\\" + dirname);
        Directory.CreateDirectory(Application.dataPath + "\\" + dirname);

        StreamWriter SW = new StreamWriter(Application.dataPath + "\\" + dirname + "\\" + filename + ".csv");
        foreach (Physics_TankNetwork.DataSeries obj in series)
            SW.WriteLine(obj.x.ToString() + "," + obj.y.ToString());

        SW.Close();
    }
    public void M_SaveTankModels(List<C_TankBlueprint> list)
    {
        foreach(C_TankBlueprint obj in list)
        {
            M_SaveTankModels(obj);
        }
    }
    public void M_SaveTankModels(C_TankBlueprint bp)
    {
        StreamWriter SW = new StreamWriter(Application.dataPath + "\\BlueprintFolder\\" + bp.m_modelname + ".tbf");

        SW.WriteLine("Modelname");
        SW.WriteLine(bp.m_modelname);
        SW.WriteLine("Model");
        SW.WriteLine(bp.m_model);
        SW.WriteLine("engineMaxPower");
        SW.WriteLine(bp.engineMaxPower);
        SW.WriteLine("EngineMaxRPM");
        SW.WriteLine(bp.engineMaxRPM);
        SW.WriteLine("EngineMaxTorque");
        SW.WriteLine(bp.EngineMaxTorque);
        SW.WriteLine("EngineShaftLength");
        SW.WriteLine(bp.EngineShaftLength);
        SW.WriteLine("EngineShaftRadius");
        SW.WriteLine(bp.EngineShaftRadius);
        SW.WriteLine("WheelShaftLength");
        SW.WriteLine(bp.WheelShaftLength);
        SW.WriteLine("WheelShaftRadius");
        SW.WriteLine(bp.WheelShaftRadius);
        SW.WriteLine("SmallerGearThickness");
        SW.WriteLine(bp.smallerGearThickness);
        SW.WriteLine("LargerGearThickness");
        SW.WriteLine(bp.largerGearThickness);
        SW.WriteLine("SmallerGearRadius");
        SW.WriteLine(bp.smallerGearRadius);
        SW.WriteLine("LargerGearRadius");
        SW.WriteLine(bp.largerGearRadius);
        SW.WriteLine("WheelThickness");
        SW.WriteLine(bp.wheelThickness);
        SW.WriteLine("WheelRadius");
        SW.WriteLine(bp.wheelRadius);
        SW.WriteLine("TrackLength");
        SW.WriteLine(bp.trackLength);
        SW.WriteLine("TrackWidth");
        SW.WriteLine(bp.trackWidth);
        SW.WriteLine("TrackThickness");
        SW.WriteLine(bp.trackThickness);
        SW.WriteLine("Ratio");
        SW.WriteLine(bp.ratio);
        SW.WriteLine("GearPowerLosage");
        SW.WriteLine(bp.powerLosage);
        SW.WriteLine("TrackDistance");
        SW.WriteLine(bp.trackDistance);
        SW.WriteLine("TankMass");
        SW.WriteLine(bp.tankMass);
        SW.WriteLine("K1");
        SW.WriteLine(bp.k1);
        SW.WriteLine("K2");
        SW.WriteLine(bp.k2);
        SW.WriteLine("END");

        SW.Close();
    }
    public void M_Delete(string filename)
    {
        File.Delete(Application.dataPath + "\\BlueprintFolder\\" + filename);
    }
    public void M_Save(L_Data datafile)
    {
        StreamWriter SW = new StreamWriter(Application.dataPath + "\\SimulationFolder\\" + datafile.m_name + ".tsf");

        SW.WriteLine(datafile.m_name);
        SW.WriteLine(datafile.m_size_x);
        SW.WriteLine(datafile.m_size_y);
        SW.WriteLine(datafile.m_groundTexture);

        for(int i = 0; i < datafile.m_objs.Count; i++)
        {
            SW.WriteLine("OBJECT" + " " + datafile.m_objs[i].pfName + " " +
                datafile.m_objs[i].pos.x + " " + datafile.m_objs[i].pos.y + " " + datafile.m_objs[i].pos.z + 
                " " + datafile.m_objs[i].rot.x + " " + datafile.m_objs[i].rot.y + " " + datafile.m_objs[i].rot.z);
        }

        SW.Close();
    }
    public List<string> M_CheckSimulations()
    {
        List<string> simulations = new List<string>();
        List<string> simulations2 = new List<string>();
        if (!Directory.Exists(Application.dataPath + "\\SimulationFolder"))
        {
            Directory.CreateDirectory(Application.dataPath + "\\SimulationFolder");
        }
        simulations.AddRange(Directory.GetFiles(Application.dataPath + "\\SimulationFolder"));
        foreach (string obj in simulations)
            if (obj.EndsWith(".tsf")) simulations2.Add(obj.Remove(obj.Length-4).Remove(0,obj.LastIndexOf('\\')+1));
        return simulations2;
    }
    public L_Data M_Load(string filename)
    {
        StreamReader SR = new StreamReader(Application.dataPath + "\\SimulationFolder\\" + filename);
        L_Data retval = new L_Data();

        retval.m_name = SR.ReadLine();
        retval.m_size_x = (int)M_ReadNumber(SR);
        retval.m_size_y = (int)M_ReadNumber(SR);
        retval.m_groundTexture = SR.ReadLine();

        string command;
        List<string> commands = new List<string>();
        retval.m_objs = new List<L_Data.obj>();
        while(!SR.EndOfStream)
        {
            command = SR.ReadLine();
            commands.Clear();
            commands.AddRange(command.Split(' '));
            if (commands[0] == "OBJECT")
            {
                retval.M_AddObj(new Vector3(M_ReadNumber(commands[2]), M_ReadNumber(commands[3]), M_ReadNumber(commands[4])),
                    new Vector3(M_ReadNumber(commands[5]), M_ReadNumber(commands[6]), M_ReadNumber(commands[7])), commands[1]);
            }
        }

        SR.Close();

        return retval;
    }
    public bool M_CheckIfTank(string modelname)
    {
        List<C_TankBlueprint> list = M_LoadTankModels();
        foreach (C_TankBlueprint obj in list)
            if (obj.m_modelname == modelname) return true;
        return false;
    }
    public float M_ReadNumber(StreamReader SR)
    {
        float retval = 0;
        float.TryParse(SR.ReadLine(), out retval);
        return retval;
    }
    public float M_ReadNumber(string word)
    {
        float retval = 0;
        float.TryParse(word, out retval);
        return retval;
    }
}
