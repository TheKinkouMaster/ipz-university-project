﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Prefab : MonoBehaviour
{
    public GameObject m_prefabAnchor;
    public GameObject m_buttonPrefab;
    private List<GameObject> m_buttons;
    public GameObject m_imagePrefab;
    private List<GameObject> m_images;
    public List<GameObject> m_tankModels;
    public void Awake()
    {
        m_buttons = new List<GameObject>();
        m_images = new List<GameObject>();
    }
    #region TPS
    public List<Material> m_materialList;
    #region Materials
    public List<Material> M_GiveMaterials()
    {
        return m_materialList;
    }
    public Material M_GiveMaterial(int index)
    {
        return m_materialList[index];
    }
    public Material M_GiveMaterial(string name)
    {
        foreach (Material obj in m_materialList)
            if (obj.name == name) return obj;
        return null;
    }
    #endregion
    #region UIElements
    public GameObject M_GiveButton()
    {
        GameObject retval = null;
        /*
        foreach(GameObject obj in m_buttons)
            if(obj.activeSelf == false)
            {
                retval = obj;
                break;
            }
            */
        if(retval == null)
        {
            retval = Instantiate(m_buttonPrefab);
        }
        else
        {
            m_buttons.Remove(retval);
        }
        retval.SetActive(true);
        return retval;
    }
    public void M_RetrieveButton(GameObject bttn)
    {
        bttn.SetActive(false);
        bttn.transform.SetParent(m_prefabAnchor.transform);
        m_buttons.Add(bttn);
    }
    public GameObject M_GiveImage()
    {
        GameObject retval = null;
        foreach (GameObject obj in m_images)
            if (obj.activeSelf == false)
            {
                retval = obj;
                break;
            }
        if (retval == null)
        {
            retval = Instantiate(m_imagePrefab);
        }
        else
        {
            m_images.Remove(retval);
        }
        retval.SetActive(true);
        return retval;
    }
    public void M_RetrieveImage(GameObject img)
    {
        img.SetActive(false);
        img.transform.SetParent(m_prefabAnchor.transform);
        m_images.Add(img);
    }
    #endregion
    #region Objects
    public List<GameObject> m_objPrefabs;
    public GameObject M_GiveObjects(string objName)
    {
        foreach (GameObject obj in m_objPrefabs)
            if (obj.name == objName) return Instantiate(obj);
        foreach (GameObject obj in m_tankModels)
            if (obj.name == objName) return Instantiate(obj);
        Debug.Log("Not found object name: " + "objName");
        return null;
    }
    public bool M_CheckIfTank(string objName)
    {
        foreach (GameObject obj in m_tankModels)
            if (obj.name == objName) return true;
        return false;
    }
    public List<string> M_GiveObjects()
    {
        List<string> retval = new List<string>();
        foreach (GameObject obj in m_objPrefabs)
            retval.Add(obj.name);
        return retval;
    }
    #endregion
    #endregion
    #region mainMenu
    public GameObject m_prefabStore;
    public C_UI m_ui;
    public List<GameObject> m_prefabsStored;
    public List<GameObject> m_prefabs;
    public void M_DisableStoredItems()
    {
        foreach (GameObject obj in m_prefabsStored)
            obj.SetActive(false);
    }
    public GameObject M_GiveObject(string objname)
    {
        GameObject obj = null;
        obj = M_WithdrawObject(objname);
        if (obj != null)
        {
            obj.SetActive(true);
            return obj;
        }
        GameObject pf = null;
        for (int i = 0; i < m_prefabs.Count; i++)
            if (m_prefabs[i].name == objname)
            {
                pf = m_prefabs[i];
            }
        if (pf == null) return null;
        else
            obj = Instantiate(pf);
        obj.name = objname;
        obj.GetComponent<B_LoadSimul>().m_ui = m_ui;
        obj.transform.SetParent(m_prefabStore.transform);
        obj.SetActive(true);
        return obj;
    }
    public GameObject M_WithdrawObject(string objname)
    {
        GameObject obj = null;
        for (int i = 0; i < m_prefabsStored.Count; i++)
            if(m_prefabsStored[i].name == objname)
            {
                obj = m_prefabsStored[i];
                m_prefabsStored.RemoveAt(i);
            }
        return obj;
    }
    public void M_AddObject(GameObject obj)
    {
        obj.transform.SetParent(m_prefabStore.transform);
        m_prefabsStored.Add(obj);
        obj.SetActive(false);
    }
    #endregion
}
