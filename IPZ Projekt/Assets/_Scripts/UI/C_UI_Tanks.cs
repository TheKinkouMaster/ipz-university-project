﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class C_UI_Tanks : MonoBehaviour {
    public C_IO m_io;
    public C_Prefab m_pf;

    public Dropdown m_DDmodels;
    public Dropdown m_DDscreens;
    public Dropdown m_DDtankmodels;
    public InputField m_IFnewName;
    public InputField m_IFengineMaxPower;
    public InputField m_IFengineMaxRPM;
    public InputField m_IFEngineShaftLength;
    public InputField m_IFEngineShaftRadius;
    public InputField m_IFEngineMaxTorque;
    public InputField m_IFWheelShaftLength;
    public InputField m_IFWheelShaftRadius;
    public InputField m_IFsmallerGearThickness;
    public InputField m_IFlargerGearThickness;
    public InputField m_IFsmallerGearRadius;
    public InputField m_IFlargerGearRadius;
    public InputField m_IFRatio;
    public InputField m_IFPowerLosage;
    public InputField m_IFwheelThickness;
    public InputField m_IFwheelRadius;
    public InputField m_IFtrackLength;
    public InputField m_IFtrackWidth;
    public InputField m_IFtrackThickness;
    public InputField m_IFTrackDistance;
    public InputField m_IFTankMass;
    public InputField m_IFk1;
    public InputField m_IFk2;
    public Text m_ModelName;

    private List<C_TankBlueprint> m_tankModels;
    public List<GameObject> m_Scenes;

    private int m_currentModel;


    public void Awake()
    {
        m_tankModels = m_io.M_LoadTankModels();
    }

    public void M_START()
    {
        M_UpdateModelDropdown();
        List<string> screens = new List<string>();
        screens.Add("Shape");
        screens.Add("Engines");
        screens.Add("Shafts");
        screens.Add("Gears");
        screens.Add("Wheel");
        screens.Add("Track");

        m_DDscreens.ClearOptions();
        m_DDscreens.AddOptions(screens);
        M_UpdateView();
    }
    public void M_UpdateModelDropdown()
    {
        List<string> options = new List<string>();
        m_tankModels = m_io.M_LoadTankModels(); 
        foreach(C_TankBlueprint obj in m_tankModels)
        {
            options.Add(obj.m_modelname);
        }
        m_DDmodels.ClearOptions();
        m_DDmodels.AddOptions(options);
        m_DDmodels.value = m_currentModel;
    }
    public void M_UpdateView()
    { 
        if (m_tankModels.Count < m_currentModel) m_currentModel = 0;
        if (m_tankModels.Count < m_currentModel && m_currentModel == 0) return;
        m_IFnewName.text = m_tankModels[m_currentModel].m_modelname;
        m_ModelName.text = m_tankModels[m_currentModel].m_modelname;
        foreach (GameObject obj in m_Scenes)
            obj.SetActive(false);
        if (m_DDscreens.value == 0)
            M_UpdateViewToShape();
        else if (m_DDscreens.value == 1)
            M_UpdateViewToEngines();
        else if (m_DDscreens.value == 2)
            M_UpdateViewToShaft();
        else if (m_DDscreens.value == 3)
            M_UpdateViewToGears();
        else if (m_DDscreens.value == 4)
            M_UpdateViewToWheel();
        else if (m_DDscreens.value == 5)
            M_UpdateViewToTrack();
    }
    public void M_UpdateViewToShape()
    {
        m_Scenes[0].SetActive(true);
        m_DDtankmodels.ClearOptions();
        List<string> m_modelNames = new List<string>();
        foreach (GameObject obj in m_pf.m_tankModels)
            m_modelNames.Add(obj.name);
        m_DDtankmodels.AddOptions(m_modelNames);
        m_DDtankmodels.value = 0;
        if (m_modelNames.Contains(m_tankModels[m_currentModel].m_model))
        {
            m_DDtankmodels.value = m_modelNames.IndexOf(m_tankModels[m_currentModel].m_model);
        }
        m_IFTankMass.text = m_tankModels[m_currentModel].tankMass.ToString();
    }
    public void M_ChangeMass()
    {
        m_tankModels[m_currentModel].tankMass = M_ReadNumber(m_IFTankMass.text);
    }
    public void M_UpdateViewToEngines()
    {
        m_Scenes[1].SetActive(true);
        M_UpdateValuesEngines();
    }
    public void M_UpdateValuesEngines()
    {
        m_IFengineMaxRPM.text = m_tankModels[m_currentModel].engineMaxRPM.ToString();
        m_IFengineMaxPower.text = m_tankModels[m_currentModel].engineMaxPower.ToString();
        m_IFEngineMaxTorque.text = m_tankModels[m_currentModel].EngineMaxTorque.ToString();
        m_IFk1.text = m_tankModels[m_currentModel].k1.ToString();
        m_IFk2.text = m_tankModels[m_currentModel].k2.ToString();
    }
    public void M_ChangeValuesEngines()
    {
        m_tankModels[m_currentModel].engineMaxPower = M_ReadNumber(m_IFengineMaxPower.text);
        m_tankModels[m_currentModel].engineMaxRPM = M_ReadNumber(m_IFengineMaxRPM.text);
        m_tankModels[m_currentModel].EngineMaxTorque = M_ReadNumber(m_IFEngineMaxTorque.text);
        m_tankModels[m_currentModel].k1 = M_ReadNumber(m_IFk1.text);
        m_tankModels[m_currentModel].k2 = M_ReadNumber(m_IFk2.text);
    }
    public void M_UpdateViewToShaft()
    {
        m_Scenes[2].SetActive(true);
        M_UpdateValuesShaft();
    }
    public void M_UpdateValuesShaft()
    {
        m_IFEngineShaftLength.text = m_tankModels[m_currentModel].EngineShaftLength.ToString();
        m_IFEngineShaftRadius.text = m_tankModels[m_currentModel].EngineShaftRadius.ToString();
        m_IFWheelShaftLength.text = m_tankModels[m_currentModel].WheelShaftLength.ToString();
        m_IFWheelShaftRadius.text = m_tankModels[m_currentModel].WheelShaftRadius.ToString();
    }
    public void M_ChangeValuesShaft()
    {
        m_tankModels[m_currentModel].EngineShaftLength = M_ReadNumber(m_IFEngineShaftLength.text);
        m_tankModels[m_currentModel].EngineShaftRadius = M_ReadNumber(m_IFEngineShaftRadius.text);
        m_tankModels[m_currentModel].WheelShaftLength = M_ReadNumber(m_IFWheelShaftLength.text);
        m_tankModels[m_currentModel].WheelShaftRadius = M_ReadNumber(m_IFWheelShaftRadius.text);
    }
    public void M_UpdateViewToGears()
    {
        m_Scenes[3].SetActive(true);
        M_UpdateValuesGears();
    }
    public void M_UpdateValuesGears()
    {
        m_IFsmallerGearRadius.text = m_tankModels[m_currentModel].smallerGearRadius.ToString();
        m_IFsmallerGearThickness.text = m_tankModels[m_currentModel].smallerGearThickness.ToString();
        m_IFlargerGearRadius.text = m_tankModels[m_currentModel].largerGearRadius.ToString();
        m_IFlargerGearThickness.text = m_tankModels[m_currentModel].largerGearThickness.ToString();
        m_IFRatio.text = m_tankModels[m_currentModel].ratio.ToString();
        m_IFPowerLosage.text = m_tankModels[m_currentModel].powerLosage.ToString();
    }
    public void M_ChangeValuesGears()
    {
        m_tankModels[m_currentModel].smallerGearRadius = M_ReadNumber(m_IFsmallerGearRadius.text);
        m_tankModels[m_currentModel].smallerGearThickness = M_ReadNumber(m_IFsmallerGearThickness.text);
        m_tankModels[m_currentModel].largerGearRadius = M_ReadNumber(m_IFlargerGearRadius.text);
        m_tankModels[m_currentModel].largerGearThickness = M_ReadNumber(m_IFlargerGearThickness.text);
        m_tankModels[m_currentModel].ratio = M_ReadNumber(m_IFRatio.text);
        m_tankModels[m_currentModel].powerLosage = M_ReadNumber(m_IFPowerLosage.text);
    }
    public void M_UpdateViewToWheel()
    {
        m_Scenes[4].SetActive(true);
        M_UpdateValuesWheel();
    }
    public void M_UpdateValuesWheel()
    {
        m_IFwheelRadius.text = m_tankModels[m_currentModel].wheelRadius.ToString();
        m_IFwheelThickness.text = m_tankModels[m_currentModel].wheelThickness.ToString();
    }
    public void M_ChangeValuesWheel()
    {
        m_tankModels[m_currentModel].wheelRadius = M_ReadNumber(m_IFwheelRadius.text);
        m_tankModels[m_currentModel].wheelThickness = M_ReadNumber(m_IFwheelThickness.text);
    }
    public void M_UpdateViewToTrack()
    {
        m_Scenes[5].SetActive(true);
        M_UpdateValuesTrack();
    }
    public void M_UpdateValuesTrack()
    {
        m_IFtrackLength.text = m_tankModels[m_currentModel].trackLength.ToString();
        m_IFtrackThickness.text = m_tankModels[m_currentModel].trackThickness.ToString();
        m_IFtrackWidth.text = m_tankModels[m_currentModel].trackWidth.ToString();
        m_IFTrackDistance.text = m_tankModels[m_currentModel].trackDistance.ToString();
    }
    public void M_ChangeValuesTrack()
    {
        m_tankModels[m_currentModel].trackLength = M_ReadNumber(m_IFtrackLength.text);
        m_tankModels[m_currentModel].trackThickness = M_ReadNumber(m_IFtrackThickness.text);
        m_tankModels[m_currentModel].trackWidth = M_ReadNumber(m_IFtrackWidth.text);
        m_tankModels[m_currentModel].trackDistance = M_ReadNumber(m_IFTrackDistance.text);
    }
    public void M_ChangeModel()
    {
        m_tankModels[m_currentModel].m_model = m_DDtankmodels.captionText.text;
    }
    public void M_CreateNewBlueprint()
    {
        C_TankBlueprint newblueprint = new C_TankBlueprint();
        newblueprint.M_Create();
        m_tankModels.Add(newblueprint);
        m_currentModel = m_tankModels.Count - 1;
        M_UpdateView();
    }
    public void M_SaveBlueprints()
    {
        m_io.M_SaveTankModels(m_tankModels);
        M_UpdateModelDropdown();
    }
    public void M_ChangeName()
    {
        m_tankModels[m_currentModel].m_modelname = m_IFnewName.text;
        M_UpdateView();
    }
    public void M_Load()
    {
        m_currentModel = m_DDmodels.value;
        M_UpdateView();
    }
    public void M_Delete()
    {
        m_io.M_Delete(m_tankModels[m_currentModel].m_modelname + ".tbf");
        m_currentModel = 0;
        M_UpdateModelDropdown();
        M_UpdateView();
    }
    
    private float M_ReadNumber(string word)
    {
        float retval = 0;
        float.TryParse(word, out retval);
        return retval;
    }
}
