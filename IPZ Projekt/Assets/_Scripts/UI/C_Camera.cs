﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
public class C_Camera : MonoBehaviour {
    public Camera m_mainMenuCamera;
    public Camera m_FPSCamera;
    public Camera m_TPSCamera;
    public Camera m_TankCamera;
    public Camera m_TankTPSCamera;
    public C_Simul m_sim;
    public enum CameraMode { fps = 0, tps = 1, menu = 2, tank = 3 , tankTPS};
    public CameraMode m_cameraMode;
    private Animator m_anim;
    private void Awake()
    {
        m_anim = m_mainMenuCamera.gameObject.GetComponent<Animator>();
        m_cameraMode = CameraMode.menu;
        M_CheckCamera();
    }

    public void M_StartGame()
    {
        m_anim.Play("State1");
    }

    public void M_ChangeScene(int targetScene)
    {
        m_anim.Play("State" + targetScene);
    }

    public void M_CheckCamera()
    {
        m_FPSCamera.gameObject.SetActive(m_cameraMode == CameraMode.fps);
        m_TPSCamera.gameObject.SetActive(m_cameraMode == CameraMode.tps);
        m_TankCamera.gameObject.SetActive(m_cameraMode == CameraMode.tank);
        m_mainMenuCamera.gameObject.SetActive(m_cameraMode == CameraMode.menu);
        m_TankTPSCamera.gameObject.SetActive(m_cameraMode == CameraMode.tankTPS);
        if (m_cameraMode != CameraMode.fps) Cursor.visible = true;
        else Cursor.visible = false;
    }
    public void M_ChangeCamera()
    {
        if (m_cameraMode == CameraMode.fps)
            m_cameraMode = CameraMode.tps;
        else if (m_cameraMode == CameraMode.tps)
            m_cameraMode = CameraMode.fps;
        else if (m_cameraMode == CameraMode.tank)
            m_cameraMode = CameraMode.tps;
        else if (m_cameraMode == CameraMode.tankTPS)
            m_cameraMode = CameraMode.tps;
        M_CheckCamera();
    }
    public void M_StartSimulation()
    {
        if (m_cameraMode == CameraMode.tank)
        {
            M_ChangeCamera();
            return;
        }
        m_cameraMode = CameraMode.tank;
        m_TankCamera.transform.parent = m_sim.M_GiveTank().GetComponent<Physics_TankInfo>().m_cameraPosition;
        m_TankCamera.transform.position = m_sim.M_GiveTank().GetComponent<Physics_TankInfo>().m_cameraPosition.position;
        m_TankCamera.transform.rotation = m_sim.M_GiveTank().GetComponent<Physics_TankInfo>().m_cameraPosition.rotation;
        m_TankCamera.transform.localScale = m_sim.M_GiveTank().GetComponent<Physics_TankInfo>().m_cameraPosition.localScale;

        m_TankTPSCamera.transform.parent = m_sim.M_GiveTank().GetComponent<Physics_TankInfo>().m_cameraTPSPosition;
        m_TankTPSCamera.transform.position = m_sim.M_GiveTank().GetComponent<Physics_TankInfo>().m_cameraTPSPosition.position;
        m_TankTPSCamera.transform.rotation = m_sim.M_GiveTank().GetComponent<Physics_TankInfo>().m_cameraTPSPosition.rotation;
        m_TankTPSCamera.transform.localScale = m_sim.M_GiveTank().GetComponent<Physics_TankInfo>().m_cameraTPSPosition.localScale;
        M_CheckCamera();
    }
    public void M_SimToTPS(CameraMode mode)
    {
        m_cameraMode = mode;
        M_CheckCamera();
    }
    public void M_RetrieveTankCamera()
    {
        m_TankCamera.transform.parent = m_FPSCamera.transform.parent;
        m_TankTPSCamera.transform.parent = m_FPSCamera.transform.parent;
    }

    List<GameObject> list = new List<GameObject>();
    public List<string> M_CaptureScreenshot(string dir, string filename)
    {
        ScreenCapture.CaptureScreenshot(dir + "/" + filename + ".png");
        list = new List<GameObject>();
        list.AddRange(GameObject.FindGameObjectsWithTag("Enemy"));
        List<string> listOfTargers = new List<string>();
        listOfTargers.Add("FILE:" + filename);
        foreach (GameObject obj in list)
            if(GeometryUtility.TestPlanesAABB(GeometryUtility.CalculateFrustumPlanes(m_TankCamera), obj.GetComponent<BoxCollider>().bounds))
            {
                Ray ray = new Ray(m_TankCamera.gameObject.transform.position, obj.transform.position - m_TankCamera.gameObject.transform.position);
                RaycastHit raycastHit = new RaycastHit();
                Physics.Raycast(ray, out raycastHit);
                Debug.Log(obj);
                if (raycastHit.collider.gameObject == obj)
                {
                    listOfTargers.Add(obj.transform.position.ToString());
                }
            }
        return listOfTargers;
    }
}
