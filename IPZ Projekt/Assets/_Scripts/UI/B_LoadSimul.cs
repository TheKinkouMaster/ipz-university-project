﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class B_LoadSimul : MonoBehaviour {
    public Text m_text;
    public C_UI m_ui;
    public void M_ChangeText(string newname)
    {
        m_text.text = newname;
    }
    public void M_LoadSimulation()
    {
        m_ui.M_StartSimulation(m_text.text + ".tsf");
    }
}
