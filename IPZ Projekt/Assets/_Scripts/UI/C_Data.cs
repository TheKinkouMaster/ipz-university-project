﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Data : MonoBehaviour {
    public List<L_Data> m_simulations;
    public GameObject m_pf_scene;
    public Transform m_scenes_anchor;

    public C_IO m_io;

    public void M_CreateNewSimul(int sizex, int sizey, string newname)
    {
        GameObject newScene = Instantiate(m_pf_scene);
        newScene.transform.parent = m_scenes_anchor;
        newScene.transform.localPosition = new Vector3(0, 0, 0);

        L_Data tmp = newScene.GetComponent<L_Data>();
        tmp.m_name = newname;
        tmp.m_size_x = sizex;
        tmp.m_size_y = sizey;

        m_io.M_Save(tmp);
        m_simulations.Add(tmp);
    }
    public void M_Save()
    {

    }

}
