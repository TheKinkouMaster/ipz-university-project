﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_FPS : MonoBehaviour {
    public float sensitivityX;
    public float sensitivityY;

    public float minimumX = -360F;
    public float maximumX = 360F;

    public float minimumY = -60F;
    public float maximumY = 60F;

    float rotationX = 0F;
    float rotationY = 0F;

    Quaternion originalRotation;
    public float velocity;
    public C_Simul m_simul;
    public float m_objectPushingVelocity;
    void Update()
    {
        //mouse
        rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
        rotationX += Input.GetAxis("Mouse X") * sensitivityX;

        rotationY = ClampAngle(rotationY, minimumY, maximumY);
        rotationX = ClampAngle(rotationX, minimumX, maximumX);

        Quaternion yQuaternion = Quaternion.AngleAxis(rotationY, Vector3.left);
        Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);

        transform.localRotation = originalRotation * xQuaternion * yQuaternion;
        //wsad
        Vector3 movevector = new Vector3();
        movevector = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        if(Input.GetKey(KeyCode.LeftShift))
            movevector = movevector * 3*velocity;
        else
            movevector = movevector * velocity;
        transform.Translate(movevector*Time.deltaTime);
        if(Input.GetMouseButtonDown(0))
        {
            if (m_takenObject == null) M_ClickObject();
            else M_ReleaseObject();
        }
        //DISTANCE
        float dist = Input.GetAxis("Mouse ScrollWheel") * m_objectPushingVelocity;
        if (Input.GetKey(KeyCode.LeftShift)) dist = dist * 3;
        m_objectAnchor.position = m_objectAnchor.position + transform.forward*dist;
        //ROTATION
        if (Input.GetKey(KeyCode.Q)) m_objectAnchor.Rotate(new Vector3(0, 1, 0), 1 * m_rotationVelocity);
        if (Input.GetKey(KeyCode.E)) m_objectAnchor.Rotate(new Vector3(0, 1, 0), -1 * m_rotationVelocity);
        //
        //if (m_takenObject != null) m_takenObject.localEulerAngles = new Vector3(0, m_takenObject.transform.rotation.eulerAngles.y, 0);
    }
    public float m_rotationVelocity;
    public float m_collisionDistance = 100;
    public Transform m_takenObject;
    public Transform m_objectAnchor;
    public GameObject m_objectsAnchor;
    public void M_ReleaseObject()
    {
        Ray ray = new Ray(m_objectAnchor.position, new Vector3(0,-1,0));
        RaycastHit raycastHit = new RaycastHit();
        Physics.Raycast(ray, out raycastHit, 100);
        if(raycastHit.transform != null)
        {
            m_takenObject.parent = m_objectsAnchor.transform;
            m_takenObject.position = raycastHit.point;
            m_takenObject.localEulerAngles = new Vector3(0, m_takenObject.localEulerAngles.y, 0);
            m_simul.M_DisableObjectsPhysics(m_takenObject.gameObject, true);
            m_takenObject = null;
        }
    }
    public void M_ClickObject()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit raycastHit = new RaycastHit();
        Physics.Raycast(ray, out raycastHit, m_collisionDistance);
        if (raycastHit.transform != null)
        {
            if (raycastHit.transform.GetComponent<PrefabsAnchor>() == null)
            {
                if (raycastHit.transform.parent.GetComponent<PrefabsAnchor>() != null)
                {
                    m_takenObject = raycastHit.transform.parent;
                    m_objectAnchor.position = transform.position + (transform.forward.normalized * raycastHit.distance);
                    m_takenObject.parent = m_objectAnchor;
                    m_simul.M_DisableObjectsPhysics(m_takenObject.gameObject, false);
                }
                else
                {
                    if (raycastHit.transform.parent.parent.GetComponent<PrefabsAnchor>() != null)
                    {
                        m_takenObject = raycastHit.transform.parent;
                        m_objectAnchor.position = transform.position + (transform.forward.normalized * raycastHit.distance);
                        m_takenObject.parent = m_objectAnchor;
                        m_simul.M_DisableObjectsPhysics(m_takenObject.gameObject, false);
                    }
                    else
                    {
                        if (raycastHit.transform.parent.parent.parent.GetComponent<PrefabsAnchor>() != null)
                        {
                            m_takenObject = raycastHit.transform.parent;
                            m_objectAnchor.position = transform.position + (transform.forward.normalized * raycastHit.distance);
                            m_takenObject.parent = m_objectAnchor;
                            m_simul.M_DisableObjectsPhysics(m_takenObject.gameObject, false);
                        }
                    }
                }
            }
            else
            {
                m_takenObject = raycastHit.transform;
                m_objectAnchor.position = transform.position + (transform.forward.normalized * raycastHit.distance);
                m_takenObject.parent = m_objectAnchor;
                m_simul.M_DisableObjectsPhysics(m_takenObject.gameObject, false);
            }
        }
    }
    void Start()
    {
        originalRotation = transform.localRotation;
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        angle = angle % 360;
        if ((angle >= -360F) && (angle <= 360F))
        {
            if (angle < -360F)
            {
                angle += 360F;
            }
            if (angle > 360F)
            {
                angle -= 360F;
            }
        }
        return Mathf.Clamp(angle, min, max);
    }
}
