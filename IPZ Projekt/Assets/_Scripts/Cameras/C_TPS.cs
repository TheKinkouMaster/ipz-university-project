﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class C_TPS : MonoBehaviour {
    public float m_velocity;
    public GameObject m_windowAnchor;
    public GameObject m_objectAnchor;
    private List<GameObject> m_buttons;
    private List<GameObject> m_images;
    public C_Prefab m_prefabs;
    public C_Simul m_simul;
    public C_IO m_io;
    public enum Window { Start, Ground, Objects, Tanks};
    public Window m_window;
    public GameObject m_windowPhotos;

    public bool registeringClicks = false;
    public List<Vector3> m_clicks;
    public void Awake()
    {
        m_buttons = new List<GameObject>();
        m_images = new List<GameObject>();
        m_window = Window.Start;
        bool registeringClicks = false;
        m_clicks = new List<Vector3>();
    }
    void Update()
    {
        //wsad
        Vector3 movevector = new Vector3();
        movevector = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Mouse ScrollWheel")*m_velocity, Input.GetAxis("Vertical"));
        if (Input.GetKey(KeyCode.LeftShift))
            movevector = movevector * 3 * m_velocity;
        else
            movevector = movevector * m_velocity;
        transform.Translate(movevector * Time.deltaTime, transform.parent);
        if(Input.GetMouseButtonDown(0) && registeringClicks == true)
        {
            M_RegisterClick();
        }
        if(Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            m_windowPhotos.SetActive(true);
            registeringClicks = false;
        }
    }
    public void M_ClickTarget()
    {

    }
    public void M_CreatePhotosFinalle()
    {
        int numberOfSamples = (int)m_io.M_ReadNumber(m_windowPhotos.GetComponentInChildren<InputField>().text);
        registeringClicks = true;
        m_simul.M_CreatePhotos(m_clicks, numberOfSamples);
        m_clicks = new List<Vector3>();
        registeringClicks = false;
        m_windowPhotos.SetActive(false);
    }
    public GameObject lolpf;
    public List<GameObject> m_markers;
    public void M_RegisterClick()
    {
        Ray ray = gameObject.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
        RaycastHit raycastHit = new RaycastHit();
        Physics.Raycast(ray, out raycastHit, 10000);
        m_clicks.Add(raycastHit.point);
        m_markers.Add(Instantiate(lolpf));
        m_markers[m_markers.Count-1].transform.position= raycastHit.point;
    }
    public void M_CreatePhotos()
    {
        if(registeringClicks == false)
        {
            m_clicks = new List<Vector3>();
            m_markers = new List<GameObject>();
            registeringClicks = true;
        }
        else if (registeringClicks == true)
        {
            registeringClicks = false;
            foreach (GameObject obj in m_markers)
                Destroy(obj);
        }
    }
    public Vector3 m_listOffset;
    public float m_buttonOffset;
    public float m_listJump;
    public void M_ChangeUI(string window)
    {
        if (window == "Ground") M_ChangeUI(Window.Ground);
        else if (window == "Objects") M_ChangeUI(Window.Objects);
        else if (window == "Tanks") M_ChangeUI(Window.Tanks);
    }
    public void M_ChangeUI(Window newwindow)
    {
        if (newwindow == m_window) return;
        if (m_window == Window.Ground) M_CloseMaterials();
        else if (m_window == Window.Objects) M_CloseObjects();
        else if (m_window == Window.Tanks) M_CloseTanks();
        m_window = newwindow;
        M_UpdateUI();
    }
    public void M_UpdateUI()
    {
        if (m_window == Window.Ground)
            M_ShowMaterials();
        else if (m_window == Window.Objects)
            M_ShowObjects();
        else if (m_window == Window.Tanks)
            M_ShowTanks();
    }
    public void M_ShowObjects()
    {
        List<string> objs = m_prefabs.M_GiveObjects();
        GameObject tmpbut;
        Vector3 pos = m_listOffset;
        foreach (string obj in objs)
        {
            tmpbut = m_prefabs.M_GiveButton();
            m_buttons.Add(tmpbut);
            tmpbut.transform.SetParent(m_windowAnchor.transform);
            tmpbut.transform.localScale = new Vector3(1, 1, 1);
            tmpbut.transform.localRotation = Quaternion.identity;
            tmpbut.transform.localPosition = new Vector3(pos.x + m_buttonOffset, pos.y, 0);
            pos = pos - new Vector3(0, m_listJump, 0);
            tmpbut.GetComponentInChildren<Text>().text = obj;
            tmpbut.AddComponent<B_Matbut>();
            tmpbut.GetComponent<B_Matbut>().M_INIT(this);
            tmpbut.GetComponent<Button>().onClick.AddListener(tmpbut.GetComponent<B_Matbut>().M_SpawnObject);
        }
    }
    public void M_ShowTanks()
    {
        GameObject tmpbut;
        Vector3 pos = m_listOffset;
        foreach (C_TankBlueprint obj in m_io.M_LoadTankModels())
        {
            tmpbut = m_prefabs.M_GiveButton();
            m_buttons.Add(tmpbut);
            tmpbut.transform.SetParent(m_windowAnchor.transform);
            tmpbut.transform.localScale = new Vector3(1, 1, 1);
            tmpbut.transform.localRotation = Quaternion.identity;
            tmpbut.transform.localPosition = new Vector3(pos.x + m_buttonOffset, pos.y, 0);
            pos = pos - new Vector3(0, m_listJump, 0);
            tmpbut.GetComponentInChildren<Text>().text = obj.m_modelname;
            tmpbut.AddComponent<B_Matbut>();
            tmpbut.GetComponent<B_Matbut>().M_INIT(this);
            tmpbut.GetComponent<Button>().onClick.AddListener(tmpbut.GetComponent<B_Matbut>().M_SpawnTank);
        }
    }
    public void M_SpawnTank(string objname)
    {
        foreach (C_TankBlueprint obj in m_io.M_LoadTankModels())
            if (obj.m_modelname == objname)
            {
                m_simul.M_SpawnTank(obj);
            }
    }
    public void M_SpawnObject(string objname)
    {
        m_simul.M_SpawnObject(objname);
    }
    public void M_CloseObjects()
    {
        foreach (GameObject obj in m_buttons)
        {
            Destroy(obj);
        }
        m_buttons = new List<GameObject>();
    }
    public void M_CloseTanks()
    {
        foreach (GameObject obj in m_buttons)
        {
            Destroy(obj);
        }
        m_buttons = new List<GameObject>();
    }
    public void M_ShowMaterials()
    {
        List<Material> materials = m_prefabs.M_GiveMaterials();
        GameObject tmpbut;
        GameObject tmpimg;
        Vector3 pos = m_listOffset;
        foreach(Material mat in materials)
        {
            //getting pfs
            tmpbut = m_prefabs.M_GiveButton();
            tmpimg = m_prefabs.M_GiveImage();
            //saving them
            m_images.Add(tmpimg);
            m_buttons.Add(tmpbut);
            //setting their parent and position
            tmpbut.transform.SetParent(m_windowAnchor.transform);
            tmpimg.transform.SetParent(m_windowAnchor.transform);

            tmpbut.transform.localScale = new Vector3(1, 1, 1);
            tmpimg.transform.localScale = new Vector3(1, 1, 1);

            tmpbut.transform.localRotation = Quaternion.identity;
            tmpimg.transform.localRotation = Quaternion.identity;

            tmpimg.transform.localPosition = pos;
            tmpbut.transform.localPosition = new Vector3(pos.x + m_buttonOffset, pos.y, 0);

            pos = pos - new Vector3(0, m_listJump, 0);
            //setting their data
            Texture2D txt = (Texture2D) mat.mainTexture;
            Sprite sprt = Sprite.Create(txt, new Rect(0.0f, 0.0f, mat.mainTexture.width, mat.mainTexture.height), new Vector2(0.5f, 0.5f), 100.0f);
            tmpimg.GetComponent<Image>().sprite = sprt;
            tmpbut.GetComponentInChildren<Text>().text = mat.name;
            tmpbut.AddComponent<B_Matbut>();
            tmpbut.GetComponent<B_Matbut>().M_INIT(this);
            tmpbut.GetComponent<Button>().onClick.AddListener(tmpbut.GetComponent<B_Matbut>().M_CLICK);
        }
    }
    public void M_CloseMaterials()
    {
        foreach(GameObject obj in m_buttons)
        {
            Destroy(obj.GetComponent<B_Matbut>());
            obj.GetComponent<Button>().onClick.RemoveAllListeners();
            m_prefabs.M_RetrieveButton(obj);
        }
        foreach (GameObject obj in m_images)
        {
            Destroy(obj.GetComponent<B_Matbut>());
            m_prefabs.M_RetrieveImage(obj);
        }
        m_buttons = new List<GameObject>();
        m_images = new List<GameObject>();
    }
    public void M_ChangeMaterial(string matname)
    {
        m_simul.M_ChangePlaneTexture(m_prefabs.M_GiveMaterial(matname));
    }
    public void M_Save()
    {
        m_simul.M_Save();
    }
    
    
}
