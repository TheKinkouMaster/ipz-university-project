﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_Body : PhysicalObject
{
    public float m_mass;
    public Vector3 m_angularVelocity;
    public Vector3 m_linearVelocity;

    public void Update()
    {
        M_ProcessPhysics();
    }

    public override void M_ProcessSignal(string package)
    {
        base.M_ProcessSignal(package);
        List<string> orders = M_SplitPackage(package);
        if(orders[0] == "PROCESSPHYSICS")
        {

        }
    }
    protected override void M_Init()
    {
        base.M_Init();
    }

    protected void M_ProcessPhysics()
    {
        //transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + (m_angularVelocity * Time.deltaTime));
        Debug.Log(transform.rotation.eulerAngles);
        transform.Rotate(m_angularVelocity * Time.deltaTime);
        transform.position += m_linearVelocity * Time.deltaTime;
    }
}
