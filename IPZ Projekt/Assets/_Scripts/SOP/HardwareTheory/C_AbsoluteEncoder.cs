﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_AbsoluteEncoder : C_Sensor {
    public enum Axis
    {
        x, y, z
    }
    public Axis m_workingAxis;
    public float m_measuredAngle;
    public float m_uncertainlity;
    protected Transform m_refAngle;
    
    protected override void M_Init()
    {
        base.M_Init();
        GameObject obj = new GameObject();
        obj.transform.parent = transform.parent;
        obj.transform.position = transform.position;
        obj.transform.rotation = transform.rotation;
        m_refAngle = obj.transform;
    }
    public override void M_ProcessSignal(string package)
    {
        base.M_ProcessSignal(package);
        List<string> commands = new List<string>();
        commands.AddRange(package.Split('|'));
        if(commands[0] == "MEASURE")
        {
            M_MeasureAngle();
            // 0 - power, 1 - controller
            m_ios[1].M_ProcessSignal(M_MergePackage(new List<string>() {"ANGLE", m_measuredAngle.ToString() }));
        }
    }

    private void M_MeasureAngle()
    {
        //Init
        Vector3 refVec = m_refAngle.forward;
        Vector3 curVec = transform.forward;
        Vector3 castAngle = new Vector3();
        //calculating vectors
        if (m_workingAxis == Axis.x)
        {
            refVec = m_refAngle.up;
            curVec = transform.up;
            curVec.x = 0;
            castAngle = m_refAngle.right;
        }
        else if (m_workingAxis == Axis.y)
        {
            refVec = m_refAngle.forward;
            curVec = transform.forward;
            curVec.y = 0;
            castAngle = m_refAngle.up;
        }
        else if (m_workingAxis == Axis.z)
        {
            refVec = m_refAngle.right;
            curVec = transform.right;
            curVec.z = 0;
            castAngle = m_refAngle.forward;
        }
        curVec.Normalize();
        //measuring angle
        m_measuredAngle = M_CastAngle(Vector3.SignedAngle(refVec, curVec, castAngle));
        //distorting signal
        m_measuredAngle = M_DistortSignal(m_measuredAngle);
    }
    private float M_DistortSignal(float value)
    {
        float retval = value;

        retval += (Random.value - 0.5f) * m_uncertainlity;

        return retval;
    }
    private float M_CastAngle(float angle)
    {
        float retval = angle;

        if (angle < 0) retval += 360;

        return retval;
    }
}
