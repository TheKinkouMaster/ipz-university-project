﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Watchdog {
    private int m_maxIterations;
    private int m_currentIteration;
    /// <summary>
    /// Initialise watchdog (necessary for it's optimal work)
    /// </summary>
    /// <param name="maxIterations">Max possible iterations watchdog can handle</param>
    public void M_Init(int maxIterations)
    {
        m_maxIterations = maxIterations;
        m_currentIteration = 0;
    }
    /// <summary>
    /// Change current iteration value.
    /// Useful for reseting watchdog.
    /// Use with caution since it may disrupt normal watchdog work
    /// </summary>
    /// <param name="newValue">new value of iteration that passed</param>
    public void M_ChangeCurrentIteration(int newValue)
    {
        m_currentIteration = newValue;
    }
    /// <summary>
    /// Changes max number of iterations allowed by watchdog.
    /// </summary>
    /// <param name="newValue">new value of max number of iterations</param>
    /// <param name="reset">if true will also reset current iteration number</param>
    public void M_ChangeMaxInterations(int newValue, bool reset)
    {
        m_maxIterations = newValue;
        if (reset == true) m_currentIteration = 0;
    }
    /// <summary>
    /// Resets iteration counter
    /// </summary>
    public void M_Reset()
    {
        m_currentIteration = 0;
    }
    /// <summary>
    /// Registers new iteration and informs about current watchdog state
    /// </summary>
    /// <returns>True - everything is fine, false - watchdog triggered</returns>
    public bool M_NextIteration()
    {
        m_currentIteration++;
        if (m_currentIteration > m_maxIterations) return false;
        else return true;
    }
}
