﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PhysicalObject : MonoBehaviour {
    public List<PhysicalObject> m_ios;
    protected bool m_init = false;

    /// <summary>
    /// The most basic method. It takes chain of commands in a form of string
    /// to split it after '|', and then analyze content. Use only this funtion
    /// to communicate between ios. Method also calls M_Init if wasn't initialised
    /// earlier
    /// </summary>
    /// <param name="package">string that represents order for object</param>
    public virtual void M_ProcessSignal(string package)
    {
        List<string> commands = new List<string>();

        if (m_init == false) M_Init();

        commands.AddRange(package.Split('|'));
    }
    /// <summary>
    /// This metod is meant to be overriden and it serves as a place
    /// where you can initialise every attribute of your class.
    /// Rember about calling base to avoid crashes
    /// </summary>
    protected virtual void M_Init()
    {
        m_ios = new List<PhysicalObject>();
        m_init = true;
    }
    /// <summary>
    /// Splits string package into list of keywords
    /// </summary>
    /// <param name="package">chain of commands</param>
    /// <returns>list of commands</returns>
    protected List<string> M_SplitPackage(string package)
    {
        List<string> retval = new List<string>();

        retval.AddRange(package.Split('|'));

        return retval;
    }
    /// <summary>
    /// Merges list of commands into package readable by other objects
    /// </summary>
    /// <param name="commands">list of commands</param>
    /// <returns>chain of commands</returns>
    protected string M_MergePackage(List<string> commands)
    {
        string retval = "";

        for(int i = 0; i < commands.Count; i++)
        {
            retval = retval + commands[i];
            if(i < commands.Count-1)retval = retval + "|";
        }
        
        return retval;
    }
    protected string M_MergePackage(string commands)
    {
        List<string> package = new List<string>
        {
            commands
        };
        return M_MergePackage(package);
    }
    /// <summary>
    /// Parses word into float
    /// </summary>
    /// <param name="word"></param>
    /// <returns></returns>
    protected float M_ParseWordToFloat(string word)
    {
        float retval = 0;

        float.TryParse(word, out retval);

        return retval;
    }
    /// <summary>
    /// Parses word into int
    /// </summary>
    /// <param name="word"></param>
    /// <returns></returns>
    protected int M_ParseWorkToInt(string word)
    {
        int retval = 0;

        Int32.TryParse(word, out retval);

        return retval;
    }
}
