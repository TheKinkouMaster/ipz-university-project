﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_RotationController : C_Software {
    protected float m_desiredAngle;

    public override void M_ProcessSignal(string package)
    {
        base.M_ProcessSignal(package);
        List<string> orders = M_SplitPackage(package);
        if (orders[0] == "ChangeAngle")
        {
            m_desiredAngle = M_ParseWordToFloat(orders[1]);
        }
        if (orders[0] == "Calculate")
        {
            
        }
    }
    protected override void M_Init()
    {
        base.M_Init();
        m_desiredAngle = 0;
    }
}
