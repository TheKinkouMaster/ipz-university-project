﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C_TurretScript : C_Software {
    //Connections
    //0 - servo, 1 - sensor, 2 - logger
    public float m_currentAngle;
    public float m_desiredAngle;
    public float m_deltaAngle;

    public float m_currentAngularVelocity;

    protected override void M_Init()
    {
        base.M_Init();
    }
    public override void M_ProcessSignal(string package)
    {
        base.M_ProcessSignal(package);
        List<string> orders = M_SplitPackage(package);
        if (orders[0] == "TICK") M_Tick();
        else if (orders[0] == "ANGLE") m_currentAngle = M_ParseWordToFloat(orders[1]);
    }


    public void M_Tick()
    {
        M_AquireDataFromSensor();
        M_CalculateSignalForServo();
        M_SendSignalToServo();
    }


    public void M_AquireDataFromSensor()
    {
        if (m_slaves[1] != null) m_slaves[1].M_ProcessSignal(M_MergePackage("MEASURE"));
    }
    public void M_SendSignalToServo()
    {

    }
    public void M_CalculateSignalForServo()
    {

    }
}
