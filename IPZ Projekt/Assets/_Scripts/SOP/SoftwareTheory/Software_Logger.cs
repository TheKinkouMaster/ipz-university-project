﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Software_Logger : C_Software {
    protected List<string> m_log;
    protected int m_logCapacity;

    public override void M_ProcessSignal(string package)
    {
        List<string> commands = M_SplitPackage(package);
        base.M_ProcessSignal(package);
        if(commands[0] == "REGISTER")
        {
            commands.RemoveAt(0);
            M_Register(M_MergePackage(commands));
        }
    }
    protected void M_Register(string command)
    {
        m_log.Add(command);
        M_EraseOverload();
    }
    protected bool M_IsOverloaded()
    {
        if (m_log.Count > m_logCapacity) return true;
        else return false;
    }
    protected void M_EraseOverload()
    {
        Watchdog wdog = new Watchdog();

        wdog.M_Init(m_logCapacity);
        while(M_IsOverloaded() && wdog.M_NextIteration()==true)
        {
            m_log.RemoveAt(0);
        }
    }
    /// <summary>
    /// Sends last logs to debugger
    /// </summary>
    /// <param name="numberOfLogs">number of last logs that will be send to Debuger. -1 for sending all of them</param>
    /// <param name="format">'s'-sending as separate communicats, 'm' - creating one package</param>
    protected void M_SendToDebugger(int numberOfLogs, char format)
    {
        string debugMessage = "";

        if (numberOfLogs == -1) numberOfLogs = m_log.Count;
        if (format == 's')
        {
            for(int i = 0; i < numberOfLogs; i++)
            {
                Debug.Log(m_log[m_log.Count - 1 - i]);
            }
        }
        else if (format == 'm')
        {
            for (int i = 0; i < numberOfLogs; i++)
            {
                debugMessage = debugMessage + m_log[m_log.Count - 1 - i];
            }
            Debug.Log(debugMessage);
        }
    }
    protected void M_SendIntoFile()
    {

    }
}
