﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour {
    public List<GameObject> m_uiLayouts;
    public List<string> m_uiLayoutsQueue;

    private void Awake()
    {
        M_ChangeScene("main");
    }

    public void M_ChangeScene(string sceneName)
    {
        if (!m_uiLayoutsQueue.Contains(sceneName)) m_uiLayoutsQueue.Add(sceneName);
        foreach (GameObject obj in m_uiLayouts)
        {
            obj.SetActive(obj.name.EndsWith(sceneName));
        }
    }
    public void M_Back()
    {
        if (m_uiLayoutsQueue.Count <= 1)
        {
            M_ChangeScene("exit");
            return;
        }
        m_uiLayoutsQueue.RemoveAt(m_uiLayoutsQueue.Count-1);
        M_ChangeScene(m_uiLayoutsQueue[m_uiLayoutsQueue.Count - 1]);
    }
}
