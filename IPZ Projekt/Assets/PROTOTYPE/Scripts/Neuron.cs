﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Neuron : MonoBehaviour {
    public List<Neuron> input_neuro;
    public List<double> input_weight;

    public List<Neuron> output_neuro;
    public List<double> output_weight;

    public double bias;
    public double value;
    public void M_CalculateValue()
    {
        double new_value = bias;
        for (int i = 0; i < input_neuro.Count; i++)
        {
            new_value += input_neuro[i].value * input_weight[i];
        }
        value = M_Sigmoid(new_value);
    }
    public double M_Sigmoid(double x)
    {
        return 1 / (1 + Math.Exp(-x));
    }
}
