﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectController : MonoBehaviour {
    public void M_SaveApp()
    {

    }
	public void M_CloseApp()
    {
        Application.Quit();
    }
    public void M_SaveAndClose()
    {
        M_SaveApp();
        M_CloseApp();
    }
}
