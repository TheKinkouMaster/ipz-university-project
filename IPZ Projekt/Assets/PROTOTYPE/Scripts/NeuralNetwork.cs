﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeuralNetwork : MonoBehaviour {
    public List<List<Neuron>> neuronLayers;
    public GameObject obj_pf;
    public void TEST()
    {
        AddLayer("lol", 7);
        AddLayer("lol", 5);
        AddLayer("lol", 9);
    }
    public void AddLayer(string type, int size)
    {
        List<Neuron> newLayer = new List<Neuron>();

        newLayer.Add(Instantiate(obj_pf).GetComponent<Neuron>());

        neuronLayers.Add(newLayer);
    }
    public void FinishNetwork()
    {

    }
}
