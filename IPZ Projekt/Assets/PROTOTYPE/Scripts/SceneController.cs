﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour {
    public List<Camera> m_cameras;

    public void M_ChangeScene(string cameraName)
    {
        if(cameraName=="CombatView")
        {
            if (Camera.main.name == "CreateCamera") cameraName = "FPSCamera";
            else if (Camera.main.name == "CreateCamera") cameraName = "CreateCamera";
            else return;
        }
        foreach (Camera obj in m_cameras)
            obj.enabled = obj.name.Equals(cameraName);
    }
    public void Awake()
    {
        M_ChangeScene("MenuCamera");
    }
}
