﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardController : MonoBehaviour {
    public UIController m_uictrl;
    public SceneController m_scnctrl;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            m_uictrl.M_Back();
            m_scnctrl.M_ChangeScene("MenuCamera");
        }
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            m_scnctrl.M_ChangeScene("CombatView");
        }
    }
}
